//
//  ConnectLoginWebViewCoordinator.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import SwiftUI
import WebKit
import Combine
import os.log

class ConnectLoginWebViewCoordinator: NSObject {

    static let loginUrl = URL(string: "login", relativeTo: URL(string: Configuration.standard.editorCloudBaseUrl))!

    private let webViewLoaded: Binding<Bool>

    private var webView: WKWebView!
    private var bootstrapNavigation: WKNavigation?

    private var connectState: ConnectLoginView.ConnectState

    init(connectState: ConnectLoginView.ConnectState, webViewLoaded: Binding<Bool>) {
        self.connectState = connectState
        self.webViewLoaded = webViewLoaded
    }

    private func webKitViewConfiguration() -> WKWebViewConfiguration {
        let config = WKWebViewConfiguration()
        config.preferences.setValue(true, forKey: "developerExtrasEnabled")
        return config
    }

    func ensureWebView() -> WKWebView {
        guard webView == nil else { return webView! }

        webView = WKWebView(frame: .zero, configuration: webKitViewConfiguration())
        webView.navigationDelegate = self

        self.bootstrapNavigation = self.loadWebView()

        return webView
    }

    private func loadWebView() -> WKNavigation? {
        let request: URLRequest
        do {
            request = try AuthIamRequest.request()
        } catch let error {
            os_log(.error, "error creating auth request: %{public}@", error.localizedDescription)
            return nil
        }

        return self.webView.load(request)
    }
}

extension ConnectLoginWebViewCoordinator: WKNavigationDelegate {

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        switch navigation {
        case bootstrapNavigation:
            webViewLoaded.wrappedValue = true
            self.bootstrapNavigation = nil
        default:
            break
        }
    }

    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {

        guard
            let url = webView.url,
            let comps = URLComponents.init(url: url, resolvingAgainstBaseURL: true),
            let loginComps = URLComponents(url: Self.loginUrl, resolvingAgainstBaseURL: true)
        else {
            return
        }
        if comps.host == loginComps.host && comps.path == loginComps.path {
            var parsingComps = URLComponents()
            parsingComps.query = comps.fragment
            guard let token = parsingComps.queryItems?.first(where: {$0.name == "access_token"})?.value else {
                os_log(.error, "seen last redirect but there is no token: %{public}@", url.absoluteString)
                return
            }
            webView.stopLoading()

            WKWebsiteDataStore.default().httpCookieStore.getAllCookies { (cookies) in
                //Extract SG cookies
                let comps = URLComponents(string: Configuration.standard.replicationBaseUrl)
                let ourCookies = cookies.filter { comps?.host?.hasSuffix($0.domain) ?? false }
                let gatewayCookies = try? SessionState.syncGatewayCookies(from: ourCookies)
                self.connectState.setLoginToken(token, cookies: gatewayCookies)
            }
        }
    }

}
