//
//  Reachability.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import SystemConfiguration
import Combine

class NetworkReachability {

    enum Error: Swift.Error {
        case systemError
    }

    enum ConnectionType: Equatable {
        case wifi
        case wwan
    }

    enum Status: Equatable {
        case notReachable
        case reachable(ConnectionType)
    }

    private var networkReachability: SCNetworkReachability

    init(networkReachability: SCNetworkReachability) throws {
        self.networkReachability = networkReachability
        try setupReachabilityCallback()
    }

    convenience init(hostName: String) throws {
        guard let reachability = SCNetworkReachabilityCreateWithName(kCFAllocatorDefault, (hostName as NSString).utf8String!) else {
            throw Error.systemError
        }
        try self.init(networkReachability: reachability)
    }

    convenience init(hostAddress: sockaddr_in) throws {
        var address = unsafeBitCast(hostAddress, to: sockaddr.self)
        guard let reachability = SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault, &address) else {
            throw Error.systemError
        }
        try self.init(networkReachability: reachability)
    }

    @Published
    private(set) var status = Status.notReachable

    func setupReachabilityCallback() throws {
        var context = SCNetworkReachabilityContext()
        context.info = Unmanaged.passUnretained(self).toOpaque()

        guard SCNetworkReachabilitySetCallback(networkReachability, { (_: SCNetworkReachability, flags: SCNetworkReachabilityFlags, info: UnsafeMutableRawPointer?) in
            if let currentInfo = info {
                let callbackSelf = Unmanaged<NetworkReachability>.fromOpaque(currentInfo).takeUnretainedValue()
                callbackSelf.status = callbackSelf.reachabilityStatus(from: flags)
            }
        }, &context) else {
            throw Error.systemError
        }

        var flags = SCNetworkReachabilityFlags()
        guard SCNetworkReachabilityGetFlags(networkReachability, &flags) else {
            throw Error.systemError
        }
        status = reachabilityStatus(from: flags)
    }

    class func networkReachabilityForInternetConnection() throws -> NetworkReachability {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        return try NetworkReachability(hostAddress: zeroAddress)
    }

    class func networkReachabilityForLocalWiFi() throws -> NetworkReachability {
        var localWifiAddress = sockaddr_in()
        localWifiAddress.sin_len = UInt8(MemoryLayout.size(ofValue: localWifiAddress))
        localWifiAddress.sin_family = sa_family_t(AF_INET)
        // IN_LINKLOCALNETNUM is defined in <netinet/in.h> as 169.254.0.0 (0xA9FE0000).
        localWifiAddress.sin_addr.s_addr = 0xA9FE0000

        return try NetworkReachability(hostAddress: localWifiAddress)
    }

    private(set) var isStarted = false

    func start() {
        guard !isStarted else { return }
        isStarted = SCNetworkReachabilityScheduleWithRunLoop(networkReachability, CFRunLoopGetMain(), CFRunLoopMode.defaultMode.rawValue)
    }

    func stop() {
        guard isStarted else { return }
        SCNetworkReachabilityUnscheduleFromRunLoop(networkReachability, CFRunLoopGetMain(), CFRunLoopMode.defaultMode.rawValue)
        isStarted = false
    }

    func reachabilityStatus(from flags: SCNetworkReachabilityFlags) -> Status {
        if !flags.contains(.reachable) {
            // The target host is not reachable.
            return .notReachable
        }
        #if os(iOS)
        if flags.contains(.isWWAN) {
            return .reachable(.wwan)
        }
        #endif
        if !flags.contains(.connectionRequired) {
            return .reachable(.wifi)
        }
        if (flags.contains(.connectionOnDemand) || flags.contains(.connectionOnTraffic)) && !flags.contains(.interventionRequired) {
            return .reachable(.wifi)
        }
        return .notReachable
    }

}
