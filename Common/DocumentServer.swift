//
//  DocumentServer.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import CouchbaseLite
import os.log
import Combine
import JWT

#if os(macOS)
import AppKit
#endif

extension CBLReplication {
    var replicationType: CloudDocumentServer.ReplicationType {
        return self.pull ? .pull : .push
    }
}

extension CBLManager {

    func doSync(block: () throws -> Void) rethrows {
        if let queue = dispatchQueue {
            try queue.sync(execute: block)
        } else {
            preconditionFailure("can not execute sync code w/o a dipatchQueue")
        }
    }

    func fetchUserProfile() throws -> CBLDocument? {
        do {
            let predicate = NSPredicate(format: "objectType = %@", "MPUserProfile")
            let database = try databaseNamed(DocumentServer.userDatabaseName)
            let builder = try CBLQueryBuilder(database: database, select: nil, wherePredicate: predicate, orderBy: nil)
            let query = builder.createQuery(withContext: nil)

            let enumerator = try query.run()
            guard enumerator.count <= 1 else { throw DocumentServer.Error.tooManyResults }

            guard let firstRow = enumerator.nextRow() else { return nil }
            return firstRow.document
        } catch let err {
            throw DocumentServer.Error.underlyingError(error: err)
        }
    }
}

extension CBLReplicationStatus: CustomDebugStringConvertible {
    public var debugDescription: String {
            switch self {
            case .stopped:
                return "stopped"
            case .offline:
                return "offline"
            case .idle:
                return "idle"
            case .active:
                return "active"
            @unknown default:
                return "unknown"
        }
    }
}

/**
 Wrapper for local CBL instance used as a backup replica and SG data replica used by the native application (e.g. project list)

 * RxDB collections synced with SG derived_data bucket sync locally to a single database (derived_data)
 * RxDB collections synced with SG projects bucket sync locally to a single database per collection (same name)
 * `DocumentServer` pre-creates derived_data and user databases
 * `DocumentServer` starts pull replication of derived_data (if logged in)
 * `DocumentServer` starts pull replication for the user database from SG projects channel "User\\(username)-projects"
 * All handlers completion blocks are called on the `managerQueue` to preserve validity of CBL objects we may
   want to pass back.

 FE uses CBL REST `_purge` to purge conflicting revisions.
 */
public class DocumentServer: ObservableObject {

    static let userDatabaseName = "user"
    static let derivedDataDatabaseName = "derived_data"

    public enum Error: Swift.Error {
        case invalidConfiguration
        case unexpectedQueryResult(row: CBLQueryRow)
        case underlyingError(error: Swift.Error)
        case replicationUnexpectedlyStillGoing
        case tooManyResults
        case invalidUserProfile
        case documentDoesNotExist
        case invalidDocument
    }

    public struct RemoteConfiguration {
        public let username: String
        public let syncGatewayBaseUrl: URL
        public let primaryBucketName: String
        public let derivedDataBucketName: String
        public let cookies: GatewayCookies?
    }

    public struct Configuration {
        public let dataDirectory: URL
        public let remote: RemoteConfiguration?
    }

    let defaultConfiguration: Configuration

    private(set) weak var enclave: DataEnclave?
    fileprivate let managerQueue: DispatchQueue //serial queue

    //A common use CBLManager. It MUST be used from its queue only (manager.doSync/manager.doAsync)
    //If CBL is accessed from an another thread a fresh instance has to be used.
    //See threading issues here https://docs.couchbase.com/couchbase-lite/1.4/objc.html
    private(set) var manager: CBLManager!

    var sessionManager: SessionManager {
        AppEnvironment.shared.sessionManager
    }

    required init(enclave: DataEnclave, configuration: Configuration) throws {
        self.enclave = enclave
        self.defaultConfiguration = configuration
        self.projectList = ProjectList(enclave: enclave)

        let queueName = configuration.dataDirectory.lastPathComponent
        managerQueue = DispatchQueue(label: "CBLManager:\(queueName)", qos: DispatchQoS.userInitiated, attributes: [/*serial*/], autoreleaseFrequency: .workItem, target: DispatchQueue.global(qos: .userInitiated))

        manager = try createManager(queue: managerQueue)

        try manager.doSync {
            try self.createDatabases(configuration: configuration)
        }
    }

    final func createManager(queue: DispatchQueue? = nil) throws -> CBLManager {
        let instance = try CBLManager(directory: defaultConfiguration.dataDirectory.path, options: nil)
        instance.dispatchQueue = queue
        return instance
    }

    private(set) var projectList: ProjectList!

    fileprivate func createDatabases(configuration: Configuration) throws {
        dispatchPrecondition(condition: .onQueue(managerQueue))
        //auto-creates the database
        let userDatabase = try manager.databaseNamed(Self.userDatabaseName)
        projectList.database = userDatabase
    }

    func applyBucketCookies(_ cookies: GatewayCookies?, completion: (() -> Void)? = nil) {
        //noop
    }

    /// Resets the database synchronously.
    public func reset(to configuration: Configuration) throws {
        dispatchPrecondition(condition: .notOnQueue(managerQueue))
        try managerQueue.sync {
            self.performStop()
            self.projectList.stop()

            for databaseName in self.manager.allDatabaseNames {
                guard let database = try? self.manager.existingDatabaseNamed(databaseName) else {
                    os_log(.info, "could not instantiate enumerated database name %{public}@", databaseName)
                    continue
                }
                try database.delete()
            }

            try self.createDatabases(configuration: configuration)
            self.performStart()
        }
    }

    fileprivate func performStart() {
        dispatchPrecondition(condition: .onQueue(managerQueue))
        projectList.start()
    }

    final func start(completion:(() -> Void)? = nil) {
        dispatchPrecondition(condition: .notOnQueue(managerQueue))
        manager.doAsync {
            self.performStart()
            completion?()
        }
    }

    fileprivate func performStop() {
        dispatchPrecondition(condition: .onQueue(managerQueue))
        projectList.stop()
    }

    final func stop(completion:(() -> Void)? = nil) {
        dispatchPrecondition(condition: .notOnQueue(managerQueue))
        manager.doAsync {
            self.performStop()
            completion?()
        }
    }

    // MARK: - Data Manipulation
    func deleteProject(documentId: String, completion: ((Swift.Error?) -> Void)? = nil) {
        dispatchPrecondition(condition: .notOnQueue(managerQueue))
        manager.doAsync {
            do {
                let database = try self.manager.existingDatabaseNamed(Self.userDatabaseName)
                guard let project = database.existingDocument(withID: documentId) else {
                    throw Error.documentDoesNotExist
                }
                try project.delete()
                completion?(nil)
            } catch let error {
                completion?(error)
            }
        }
    }

    func renameProject(documentId: String, to newName: String, completion: ((Swift.Error?) -> Void)? = nil) {
        dispatchPrecondition(condition: .notOnQueue(managerQueue))
        manager.doAsync {
            do {
                let database = try self.manager.existingDatabaseNamed(Self.userDatabaseName)
                guard let project = database.existingDocument(withID: documentId) else {
                    throw Error.documentDoesNotExist
                }
                guard var properties = project.properties else {
                    throw Error.invalidDocument
                }
                properties["title"] = newName
                try project.putProperties(properties)
                completion?(nil)
            } catch let error {
                completion?(error)
            }
        }
    }

    @inline(__always)
    final func assertManagerQueue() {
        dispatchPrecondition(condition: .onQueue(managerQueue))
    }

}

// MARK: - Local Document Server
class LocalDocumentServer: DocumentServer {

    required init(enclave: DataEnclave, configuration: Configuration) throws {
        try super.init(enclave: enclave, configuration: configuration)
        try setupLocalSession()
    }

    final func setupLocalSession() throws {
        var sessionState: SessionState! = nil
        try manager.doSync {
            sessionState = try ensureLocalSession()
        }
        sessionManager.setSession(state: sessionState, in: enclave!.scope)
    }

    private func localJWT(from properties: [String: Any]) throws -> String {
        guard
            let userId = properties["userID"] as? String,
            let profileId = properties["_id"] as? String
        else {
            os_log(.error, "insufficient properties to build JWT")
            throw Error.invalidUserProfile
        }

        let claims: [String: Any] = [
            "userId": userId,
            "userProfileId": profileId,
            "appId": "manuscripts",
            "email": (properties["email"] as? String) ?? "dummy@manuscripts.app",
            "expiry": Int(Date().timeIntervalSince1970) + 86400 * 3650,
            "iat": Int(Date().timeIntervalSince1970)
        ]

        let jwt = JWT.encode(claims: claims, algorithm: .hs256(Data(repeating: 0, count: 32)))
        return jwt
    }

    private func ensureLocalSession() throws -> SessionState {
        dispatchPrecondition(condition: .onQueue(managerQueue))
        var jwtProperties: [String: Any]
        if let userProfile = try manager.fetchUserProfile() {
            guard let profileProperties = userProfile.currentRevision?.properties else {
                preconditionFailure("no user properties")
            }
            jwtProperties = profileProperties
        } else {
            let database = try manager.databaseNamed(Self.userDatabaseName)
            let uuidString = UUID().uuidString
            let profileIdentifier = "MPUserProfile:\(uuidString)"
            let userProfile = database[profileIdentifier]

            let properties: [String: Any] = [
                "bibliographicName": [
                    "family": "Local",
                    "given": "User",
                    "_id": "MPBibliographicName:\(UUID().uuidString)",
                    "objectType": "MPBibliographicName"],
                "objectType": "MPUserProfile",
                "userID": "User_\(uuidString)"
            ]

            try userProfile?.putProperties(properties)
            jwtProperties = properties
            jwtProperties["_id"] = profileIdentifier
        }
        return try SessionState(token: try localJWT(from: jwtProperties))
    }

    fileprivate override func createDatabases(configuration: Configuration) throws {
        dispatchPrecondition(condition: .onQueue(managerQueue))
        try super.createDatabases(configuration: configuration)
    }

}

// MARK: - Cloud Document Server
class CloudDocumentServer: DocumentServer {

    public typealias ErrorHandler = (_ database: CBLDatabase, _ replication: CBLReplication, Error) -> Void
    public typealias StatusUpdateHandler = (_ database: CBLDatabase, _ replication: CBLReplication) -> Void

    public enum ReplicationType: CustomDebugStringConvertible {
        case pull
        case push

        public var debugDescription: String {
            switch self {
            case .pull: return "pull"
            case .push: return "push"
            }
        }
    }

    enum ReplicationStatus: Equatable {
        case stopped
        case running
        case failed
    }

    /// All handlers are called on the managerQueue.
    /// This ensure all manager related objects passed as callback arguments are still valid.
    var errorHandler: ErrorHandler?
    var statusUpdateHandler: StatusUpdateHandler?

    @Published
    private(set) var replicationStatus = ReplicationStatus.stopped

    private var replicationChangeCancellable: AnyCancellable?

    public required init(enclave: DataEnclave, configuration: Configuration) throws {
        try super.init(enclave: enclave, configuration: configuration)

        //register for replication state changes
        replicationChangeCancellable = observeReplicationChanges()
    }

    // MARK: - Replication Status
    func observeReplicationChanges() -> AnyCancellable {
        return NotificationCenter.default.publisher(for: .cblReplicationChange)
            .sink { [weak self] (notification) in
                guard let this = self else { return }
                dispatchPrecondition(condition: .onQueue(this.managerQueue))
                guard let replication = notification.object as? CBLReplication else {
                    os_log(.error, "unexpected .cblReplicationChange object")
                    return
                }

                if let error = replication.lastError {
                    this.errorHandler?(replication.localDatabase, replication, .underlyingError(error: error))
                    this.replicationStatus = .failed
                }
                self?.statusUpdateHandler?(replication.localDatabase, replication)
            }
    }

    // MARK: Replications

    ///Keeps a list of replications per-bucket we need to reset SG cookies
    private var bucketReplications = [String: [CBLReplication]]()

    private var allReplications: [CBLReplication] {
        bucketReplications.values.flatMap { $0 }
    }

    override fileprivate func createDatabases(configuration: Configuration) throws {
        dispatchPrecondition(condition: .onQueue(managerQueue))
        try super.createDatabases(configuration: configuration)
        try createReplications(configuration: configuration)
        performApplyBucketCookies(configuration.remote?.cookies)
    }

    private func createReplications(configuration: Configuration) throws {
        dispatchPrecondition(condition: .onQueue(managerQueue))

        func append(replication: CBLReplication, to bucket: String) {
            bucketReplications.merge([bucket: [replication]]) { $0 + $1 }
        }

        self.bucketReplications.removeAll()

        guard let remote = configuration.remote else {
            return
        }

        //user to primary bucket replications
        let userDatabase = try manager.databaseNamed(Self.userDatabaseName)
        let primaryUrl = remote.syncGatewayBaseUrl.appendingPathComponent(remote.primaryBucketName)
        //continuous pull of the user collection
        let userPullReplication = userDatabase.createPullReplication(primaryUrl)
        userPullReplication.continuous = true
        userPullReplication.channels = [
            "User_\(remote.username)-projects",
            "User_\(remote.username)-readwrite"
        ]
        append(replication: userPullReplication, to: remote.primaryBucketName)
        //continuous push of the user collection
        let userPushReplication = userDatabase.createPushReplication(primaryUrl)
        userPushReplication.continuous = true
        append(replication: userPushReplication, to: remote.primaryBucketName)

        //continuous pull of derived data
        let derivedDatabase = try manager.databaseNamed(Self.derivedDataDatabaseName)
        let derivedDataUrl = remote.syncGatewayBaseUrl.appendingPathComponent(remote.derivedDataBucketName)
        let derivedDataReplication = derivedDatabase.createPullReplication(derivedDataUrl)
            derivedDataReplication.continuous = true
        append(replication: derivedDataReplication, to: remote.derivedDataBucketName)
    }

    // MARK: - Replication Lifecycle
    override fileprivate func performStart() {
        super.performStart()
        let replications = allReplications
        replications.forEach { $0.start() }
        //after start optimistically assume replications are running
        replicationStatus = replications.isEmpty ? .stopped : .running
    }

    override fileprivate func performStop() {
        dispatchPrecondition(condition: .onQueue(managerQueue))
        super.performStop()
        allReplications.forEach { $0.stop() }
        replicationStatus = .stopped
    }

    // MARK: - Accept Cookies
    private func performApplyBucketCookies(_ cookies: GatewayCookies?) {
        dispatchPrecondition(condition: .onQueue(managerQueue))
        for (bucket, replications) in self.bucketReplications {
            guard let cookie = cookies?[bucket] else { continue }
            replications.forEach {
                $0.setCookieNamed(cookie.name, withValue: cookie.value, path: cookie.path, expirationDate: nil, secure: cookie.isSecure)
            }
        }
    }

    override func applyBucketCookies(_ cookies: GatewayCookies?, completion: (() -> Void)? = nil) {
        managerQueue.async {
            self.performApplyBucketCookies(cookies)
            self.performStart()
            completion?()
        }
    }

}
