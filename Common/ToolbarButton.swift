//
//  ToolbarButton.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import SwiftUI

struct ToolbarButton: View {

    struct ButtonPosition: OptionSet {
        var rawValue: Int
        static let leading = ButtonPosition(rawValue: 1 << 0)
        static let trailing = ButtonPosition(rawValue: 1 << 1)
    }

    static let cornerRadius: CGFloat = 4

    @EnvironmentObject var toolbarState: ToolbarState

    let buttonPosition: ButtonPosition
    let toolbarItem: ToolbarItem
    let itemState: ToolbarItem.State

    func maskRect(size: CGSize) -> some Shape {
        var path = Path()

        path.move(to: CGPoint(x: size.width / 2, y: 0))
        if buttonPosition.contains(.trailing) {
            path.addLine(to: CGPoint(x: size.width - Self.cornerRadius, y: 0))
            path.addArc(center: CGPoint(x: size.width - Self.cornerRadius, y: Self.cornerRadius), radius: Self.cornerRadius, startAngle: Angle(degrees: 270), endAngle: Angle(degrees: 0), clockwise: false)
            path.addLine(to: CGPoint(x: size.width, y: size.height - Self.cornerRadius))
            path.addArc(center: CGPoint(x: size.width - Self.cornerRadius, y: size.height - Self.cornerRadius), radius: Self.cornerRadius, startAngle: Angle(degrees: 0), endAngle: Angle(degrees: 90), clockwise: false)
            path.addLine(to: CGPoint(x: size.width / 2, y: size.height))
        } else {
            path.addLine(to: CGPoint(x: size.width, y: 0))
            path.addLine(to: CGPoint(x: size.width, y: size.height))
            path.addLine(to: CGPoint(x: size.width / 2, y: size.height))
        }

        if buttonPosition.contains(.leading) {
            path.addLine(to: CGPoint(x: Self.cornerRadius, y: size.height))
            path.addArc(center: CGPoint(x: Self.cornerRadius, y: size.height - Self.cornerRadius), radius: Self.cornerRadius, startAngle: Angle(degrees: 90), endAngle: Angle(degrees: 180), clockwise: false)
            path.addLine(to: CGPoint(x: 0, y: Self.cornerRadius))
            path.addArc(center: CGPoint(x: Self.cornerRadius, y: Self.cornerRadius), radius: Self.cornerRadius, startAngle: Angle(degrees: 180), endAngle: Angle(degrees: 270), clockwise: false)
        } else {
            path.addLine(to: CGPoint(x: 0, y: size.height))
            path.addLine(to: CGPoint(x: 0, y: 0))
        }

        path.closeSubpath()

        return path
    }

    var body: some View {
        Button(action: {self.toolbarState.signalAction(for: self.toolbarItem)}, label: {
            Image(toolbarItem.imageName ?? "__invalid")
                .renderingMode(.original)
                .padding(EdgeInsets(top: 4, leading: 6, bottom: 4, trailing: 6))
                .opacity(self.itemState.isEnabled ? 1.0 : 0.2)
        })
            .zIndex(self.itemState.isEnabled ? 2 : 1)
            .disabled(!self.itemState.isEnabled)
            .background(self.itemState.isActive ? Color(white: 0.93) : Color.white)
            .mask(GeometryReader { geo in
                self.maskRect(size: geo.size)
                    .fill(Color.black)
            })
            .overlay(GeometryReader { geo in
                self.maskRect(size: geo.size)
                    .stroke(self.itemState.isEnabled ? Color(white: 0.80) : Color(white: 0.95), lineWidth: 1)
            })
    }
}
