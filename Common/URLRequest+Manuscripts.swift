//
//  URLRequest+Manuscripts.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

extension URLRequest {

    enum HTTPMethod: String {
        case get
        case put
        case post
        case delete
    }

    struct HTTPHeader {
        static let contentType = "Content-Type"
        static let accept = "Accept"
        static let manuscriptsAppId = "Manuscripts-app-id"
        static let manuscriptsAppSecret = "Manuscripts-app-secret"
        static let authorization = "Authorization"
    }

    struct ContentType {
        static let json = "application/json"
    }

    mutating func setPayload<T: Encodable>(json: T, method: HTTPMethod = .post, encoder: JSONEncoder? = nil, token: String? = nil) throws {
        let currentEncoder = encoder ?? JSONEncoder()
        httpBody = try currentEncoder.encode(json)
        setValue(ContentType.json, forHTTPHeaderField: HTTPHeader.contentType)
        setValue(ContentType.json, forHTTPHeaderField: HTTPHeader.accept)

        if let jwtToken = token {
            setValue("Bearer \(jwtToken)", forHTTPHeaderField: HTTPHeader.authorization)
        }

        httpMethod = method.rawValue.uppercased()
    }
}
