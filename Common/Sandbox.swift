//
//  Sandbox.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

struct Sandbox {

    #if os(macOS)
    private static var isSandboxedCached: Bool?

    static var isSandboxed: Bool {
        if let cached = isSandboxedCached {
            return cached
        }

        isSandboxedCached = {
            let bundleURL = Bundle.main.bundleURL as CFURL
            var staticCode: SecStaticCode?
            let kSecCSDefaultFlags = SecCSFlags(rawValue: 0)

            guard SecStaticCodeCreateWithPath(bundleURL, kSecCSDefaultFlags, &staticCode) == errSecSuccess else {
                return false
            }

            guard SecStaticCodeCheckValidityWithErrors(staticCode!, SecCSFlags(rawValue: kSecCSBasicValidateOnly), nil, nil) == errSecSuccess else {
                return false
            }

            let requirementString = "entitlement[\"com.apple.security.app-sandbox\"] exists" as CFString
            var sandboxRequirement: SecRequirement?

            guard SecRequirementCreateWithString(requirementString, kSecCSDefaultFlags, &sandboxRequirement) == errSecSuccess  else {
                return false
            }

            guard SecStaticCodeCheckValidityWithErrors(staticCode!, SecCSFlags(rawValue: kSecCSBasicValidateOnly), sandboxRequirement, nil) == errSecSuccess else {
                return false
            }

            return true
        }()

        return isSandboxedCached!
    }
    #elseif os(iOS)
    static var isSandboxed: Bool { true }
    #else
    #error("unsupported platform")
    #endif

}
