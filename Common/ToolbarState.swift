//
//  ToolbarState.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import SwiftUI
import Combine
import os.log

enum ToolbarItem: String, StoredStateItem {

    struct State: StoredStateValue {
        let isEnabled: Bool
        let isActive: Bool

        static let defaultValue = State(isEnabled: false, isActive: false)
    }

    typealias Message = [String: [String: Any]]

    enum SelectionMode {
        case momentary
        case single
        case multiple
    }

    case bold
    case italic
    case underline

    case `subscript`
    case superscript

    case bulletList = "bullet_list"
    case orderedList = "ordered_list"

    case citation

    case figureElement = "figure_element"
    case tableElement = "table_element"
    case equationElement = "equation_element"
    case listingElement = "listing_element"

    case highlight

    var imageName: String? {
        switch self {
        case .bold: return "ToolbarIconBold"
        case .italic: return "ToolbarIconItalic"
        case .underline: return "ToolbarIconUnderline"
        case .subscript: return "ToolbarIconSubscript"
        case .superscript: return "ToolbarIconSuperscript"
        case .bulletList: return "ToolbarIconUnorderedList"
        case .orderedList: return "ToolbarIconOrderedList"
        case .citation: return "ToolbarIconCitation"
        case .figureElement: return "ToolbarIconFigure"
        case .tableElement: return "ToolbarIconTable"
        case .equationElement: return "ToolbarIconEquation"
        case .listingElement: return "ToolbarIconCodeSnippet"
        case .highlight: return "ToolbarIconHighlight"
        //default:
            //os_log(.error, "no image for toolbar item %{public}@", self.rawValue)
            //return nil
        }
    }

    var label: String {
        switch self {
        case .bold: return "Bold"
        case .italic: return "Italic"
        case .underline: return "Underline"
        case .subscript: return "Subscript"
        case .superscript: return "Superscript"
        case .bulletList: return "Bullets"
        case .orderedList: return "Numbers"
        case .citation: return "Citation"
        case .figureElement: return "Figure"
        case .tableElement: return "Table"
        case .equationElement: return "Equation"
        case .listingElement: return "Code"
        case .highlight: return "Highlight"
        }
    }

    func itemState(from message: Message) -> State? {
        guard let messageItem = message[self.rawValue] else {
            os_log(.debug, "no state data for toolbar item '%{public}s'", self.rawValue)
            return nil
        }
        let enabled = messageItem["enabled"] as? Bool ?? false
        let active = messageItem["active"] as? Bool ?? false
        return State(isEnabled: enabled, isActive: active)
    }

    //default toolbar grouping
    typealias ToolbarItemGroup = (items: [ToolbarItem], selection: SelectionMode, label: String)
    static let groups: KeyValuePairs<String, ToolbarItemGroup> = [
        "group1": (items: [.bold, .italic, .underline], selection: .multiple, label: "Formatting"),
        "group2": (items: [.subscript, .superscript], selection: .multiple, label: "Position"),
        "group3": (items: [.bulletList, .orderedList], selection: .multiple, label: "Insert List"),
        "group4": (items: [.citation, .highlight], selection: .momentary, label: "Citations & Highlights"),
        "group5": (items: [.figureElement, .tableElement, .equationElement, .listingElement], selection: .momentary, label: "Insert Object")
    ]
}

class ToolbarState: StateStorage<ToolbarItem> {
    private(set) var itemSignal = PassthroughSubject<ToolbarItem, Never>()

    func signalAction(for item: ToolbarItem) {
        itemSignal.send(item)
    }

    #if DEBUG
    override func assertMessageFormat(_ message: ToolbarItem.Message) {
        var allMessageKeys = Set(message.keys)
        for item in ToolbarItem.allCases {
            allMessageKeys.remove(item.rawValue)
        }
        assert(allMessageKeys.isEmpty, "unknown toolbar item keys: \(allMessageKeys.joined(separator: ", "))")
    }
    #endif

}
