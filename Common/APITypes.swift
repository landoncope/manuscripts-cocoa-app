//
//  APITypes.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

enum APIError: LocalizedError {
    case missingBundleIdentifier
    case invalidParameter(String)
    case invalidServerResponse
    case authenticationRequired
    case invalidCredentials
    case invalidCookies
    case invalidJWT
    case invalidUrl(String)
    case serverError(ServerError)
    case underlyingError(Swift.Error)
}

struct ServerError: Error, Decodable {
    let error: ErrorBody?
}

struct ErrorBody: Decodable {
    let error: String
}

struct ErrorDetails: Decodable {
    let name: String
}
