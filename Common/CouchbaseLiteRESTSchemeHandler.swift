//
//  ResourceURLSchemeHandler.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import WebKit
import os.log
import Combine

/**
 This class is a bridge between XHR sent by the WebKit app and CBL custom protocol. The main purpose is to maintain a local replica of PouchDB data.

 Pouchdb-adapter-http is sending a subset of CouchDB REST API calls to the registered native schema handler (`app-resources`) defined in `ResourceURLSchemeHandler` which uses `CouchbaseLiteRESTSchemeHandler` as a _plugin_ registered for _cbl-rest_ first path component.

 `CouchbaseLiteRESTSchemeHandler` remaps the incoming request to CBL custom protocol/schema and sends a respose it obtains from CBL back to the WebKit app.
*/
class CouchbaseLiteRESTSchemeHandler: NSObject, WKURLSchemeHandler {

    enum Error: Swift.Error {
        case noURL
        case invalidURL
        case noResponse
        case invalidResponse
        case networkEWrror(Swift.Error)
    }

    let documentServer: DocumentServer

    init(documentServer: DocumentServer) {
        self.documentServer = documentServer
        super.init()
    }

    /**
     We need to keep a track of pending tasks (WKURLSchemeTask) in order to handle task cancelation initiated from WEbKit (e.g. as a result of timeout).

     If webView(_: urlSchemeTask:) is called we need to ignore the future response we still are receiving from CBL custom schema - we do it by cancelling Combine `sink` registered with the `cbl://` schema request.

     WKURLSchemeTask is neither Equotable nor Hashable, we need to use an array and full-scan
     This assumes the number of concurrent tasks is low
     */
    typealias TaskItem = (task: WKURLSchemeTask, started: Date, sink: AnyCancellable)
    var pendingTasks = [TaskItem]()

    private func index(of task: WKURLSchemeTask) -> Int? {
        return pendingTasks.firstIndex(where: {$0.task === task})
    }

    private func forgetTask(_ task: WKURLSchemeTask) {
        dispatchPrecondition(condition: .onQueue(.main))
        guard let index = index(of: task) else {
            assertionFailure("can not find pending task item")
            return
        }
        pendingTasks.remove(at: index)
        #if LOG_SCHEME_HANDLER
        os_log(.debug, "forgot task for %{public}@, remainig task count %d", task.request.url?.absoluteString ?? "no url", pendingTasks.count)
        #endif
    }

    func webView(_ webView: WKWebView, start urlSchemeTask: WKURLSchemeTask) {
        guard urlSchemeTask.request.url != nil else {
            urlSchemeTask.didFailWithError(Error.noURL)
            assertionFailure("no url in handler?")
            return
        }

        self.respondToTask(urlSchemeTask)
    }

    func webView(_ webView: WKWebView, stop urlSchemeTask: WKURLSchemeTask) {
        guard let itemIndex = index(of: urlSchemeTask) else {
            assertionFailure("stopped task we do not store")
            return
        }
        //cancel processing
        let item = pendingTasks[itemIndex]
        item.sink.cancel()

        #if LOG_SCHEME_HANDLER
        os_log(.debug, "url task stopped after %g (%{public}@)", Date().timeIntervalSince(item.started), urlSchemeTask.request.url?.absoluteString ?? "no url")
        #endif

        forgetTask(urlSchemeTask)
    }

    let urlSession: URLSession = {
        let config = URLSessionConfiguration.ephemeral
        if let cls = NSClassFromString("CBL_URLProtocol") {
            config.protocolClasses = [cls as AnyClass]
        }
        let session = URLSession(configuration: config)
        return session
    }()

    private static let syncGatewayOnlyParams: Set<String> = ["filter", "channels"]
    static let pathPrefix = "cbl-rest"

    /// This getter does the reverse of what is done in +[CBL_URLProtocol HTTPURLForServerURL]
    /// We need to extract a portion of document server's host to use in a translated cbl:// url
    /// used to access the CBL rest url protocol
    private lazy var cblUrlHost: String = {
        var retval: String?
        documentServer.manager.doSync {
            if let comps = URLComponents(url: documentServer.manager.internalURL, resolvingAgainstBaseURL: true),
                let range = comps.host?.range(of: ".couchbase.") {
                retval = String(comps.host![..<range.lowerBound])
            }
        }
        return retval ?? "lite"
    }()

    private func request(from urlSchemeTask: WKURLSchemeTask) throws -> URLRequest {

        guard
            let requestedUrl = urlSchemeTask.request.url,
            let urlComponents = URLComponents(url: requestedUrl, resolvingAgainstBaseURL: true),
            urlComponents.path.hasPrefix("/\(Self.pathPrefix)")
        else {
            throw Error.invalidURL
        }

        let index = urlComponents.path.index(urlComponents.path.startIndex, offsetBy: "/\(Self.pathPrefix)".count)
        let forwardedPath = urlComponents.path[index...]

        var forwardedComponents = URLComponents()
        forwardedComponents.scheme = "cbl"
        forwardedComponents.host = cblUrlHost
        forwardedComponents.port = urlComponents.port
        forwardedComponents.path = String(forwardedPath)
        forwardedComponents.queryItems = urlComponents.queryItems

        guard let url = forwardedComponents.url else {
            preconditionFailure("unexpected error where we should obtain url safely")
        }

         var req = URLRequest(url: url,
                              cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalCacheData,
                              timeoutInterval: TimeInterval.infinity)
        req.httpMethod = urlSchemeTask.request.httpMethod

        req.httpBody = urlSchemeTask.request.httpBody
        req.allHTTPHeaderFields = urlSchemeTask.request.allHTTPHeaderFields

        #if LOG_SCHEME_HANDLER
        os_log(.debug, "cbl request: %{public}@ %{public}@", req.httpMethod!, req.url!.absoluteString)
        #endif

        return req
    }

    private func respondToTask(_ urlSchemeTask: WKURLSchemeTask) {
        //hack - normalize the url
        let req: URLRequest
        do {
            req = try request(from: urlSchemeTask)
        } catch let error {
            urlSchemeTask.didFailWithError(error)
            return
        }

        #if LOG_SCHEME_HANDLER
        os_log(.debug, "cbl-handler requested: %{public}@", req.url!.path)
        #endif

        let startedAt = Date()
        let sink = urlSession.dataTaskPublisher(for: req)
            .tryMap { taskOutput -> (translatedResponse: HTTPURLResponse, data: Data) in
                 guard
                     let httpResponse = taskOutput.response as? HTTPURLResponse,
                     200..<500 ~= httpResponse.statusCode else {
                         throw Error.invalidResponse
                 }
                guard let response = HTTPURLResponse(url: urlSchemeTask.request.url!, statusCode: httpResponse.statusCode, httpVersion: nil, headerFields: httpResponse.allHeaderFields as? [String: String])
                 else {
                     throw Error.invalidResponse
                 }
                return (translatedResponse: response, data: taskOutput.data)
            }
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { completion in
                self.completeTask(urlSchemeTask, request: req, startedAt: startedAt, completion: completion)
            }, receiveValue: {value in
                self.sendResponse(value.translatedResponse, data: value.data, to: urlSchemeTask, startedAt: startedAt)
            })

        pendingTasks.append((task: urlSchemeTask, started: Date(), sink: sink))
    }

    private func completeTask(_ urlSchemeTask: WKURLSchemeTask, request: URLRequest, startedAt: Date, completion: Subscribers.Completion<Swift.Error>) {
        switch completion {
        case let .failure(error):
            os_log(.error, "cbl-handler failed (%g): %{public}@, %{public}@", Date().timeIntervalSince(startedAt), request.url!.absoluteString, error.localizedDescription)
            urlSchemeTask.didFailWithError(error)
        case .finished:
            #if LOG_SCHEME_HANDLER
            os_log(.debug, "cbl-handler finished (%g): %{public}@", Date().timeIntervalSince(startedAt), request.url!.absoluteString)
            #endif
        }
        self.forgetTask(urlSchemeTask)
    }

    private func sendResponse(_ response: HTTPURLResponse, data: Data, to task: WKURLSchemeTask, startedAt: Date) {
        #if LOG_SCHEME_HANDLER
        os_log(.debug, "cbl-handler sending response to (%g): %{public}@ %{public}@", Date().timeIntervalSince(startedAt), task.request.httpMethod!, task.request.url!.absoluteString)
        os_log(.debug, "cbl-handler response %{public}@", response)
        os_log(.debug, "cbl-handler response data %{public}@", String(data: data, encoding: .utf8) ?? "_invalid string_")
        #endif

        task.didReceive(response)
        task.didReceive(data)
        task.didFinish()
    }
}
