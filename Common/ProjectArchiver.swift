//
//  ProjectArchiver.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import ZIPFoundation
import CouchbaseLite

extension CBLDatabase {
    ///In reality CBLDatabase.inTransaction is always synchronous.
    ///We only need to fix imprefect Obj-C bridging
    @discardableResult
    func inTransactionSync(block: () throws -> Bool) rethrows -> Bool {
        return try withoutActuallyEscaping(block) { escapingBlock in
            var blockError: Error?
            let retval = inTransaction {
                do {
                    return try escapingBlock()
                } catch let error {
                    blockError = error
                    return false
                }
            }
            if let realError = blockError { throw realError }
            return retval
        }
    }
}

class ProjectArchiver {

    typealias JSONDict = [String: Any]

    enum ArchiveError: LocalizedError {
        case invalidFormat
        case fatalError
        case unsupportedVersion
    }

    // MARK: Archiving

    /// Writes a project collection to a file. This is an atomic operation managed by NSDocument.writeSafely
    func archive(project: Project, to fileUrl: URL, transactionConfirmedBlock: @escaping () -> Void) throws {
        //create a dedicated manager to obey CBL concurrency guidelines
        let manager = try project.enclave.documentServer.createManager()

        guard let archive = Archive(url: fileUrl, accessMode: .create) else {
            throw ArchiveError.fatalError
        }

        //save master record
        let userDatabase = try manager.existingDatabaseNamed("user")
        try userDatabase.inTransactionSync {
            guard let projectDocument = userDatabase.document(withID: project.documentId) else {
                throw ArchiveError.fatalError
            }
            let sequence = CollectionOfOne(projectDocument)
            try self.storeDocuments(sequence, project: project, to: archive, at: "project.json")
            return true
        }

        //save the project collection/database
        let collectionName = (project.url.lastPathComponent as NSString).deletingPathExtension

        let projectDatabase = try manager.existingDatabaseNamed(collectionName)

        //this block is not really escaping
        try projectDatabase.inTransactionSync {
            //write header
            var indexDict = JSONDict()
            indexDict["version"] = 1
            let indexData = try JSONSerialization.data(withJSONObject: indexDict, options: [])

            try archive.addEntry(with: "index.json", type: .file, uncompressedSize: UInt32(indexData.count), compressionMethod: .deflate) { (position, size) in
                return indexData[position..<(position + size)]
            }
            //write all project collection documents
            let query = projectDatabase.createAllDocumentsQuery()
            let enumerator = try query.run()

            //this mostly unblocks UI
            transactionConfirmedBlock()

            let sequence = AnySequence {
                AnyIterator {
                    enumerator.nextRow()?.document as CBLDocument?
                }
            }
            try self.storeDocuments(sequence, project: project, to: archive, at: "content.json")
            return true
        }
    }

    private func storeDocuments<S>(_ documents: S, project: Project, to archive: Archive, at path: String) throws where S: Sequence, S.Element == CBLDocument {
        var attachmentUrls = Set<URL>()
        var documentsJson = [JSONDict]()
        for document in documents {
            let (documentDictionary, documentAttachmentUrls) = try dataOfDocument(document)

            //append data
            documentsJson.append(documentDictionary)
            attachmentUrls.formUnion(documentAttachmentUrls)
        }
        //store documents/revisions
        let documentsData = try JSONSerialization.data(withJSONObject: documentsJson, options: [.prettyPrinted])
        try archive.addEntry(with: path, type: .file, uncompressedSize: UInt32(documentsData.count), compressionMethod: .deflate) { (position, size) in
            return documentsData[position..<(position + size)]
        }
        //store all attachments
        for attachmentUrl in attachmentUrls {
            try archive.addEntry(with: "attachments/\(attachmentUrl.lastPathComponent)", relativeTo: project.url)
        }
    }

    private func dataOfDocument(_ document: CBLDocument) throws -> (JSONDict, Set<URL>) {
        let history = try document.getRevisionHistory()

        var allRevisions = [JSONDict]()
        var documentDictionary = JSONDict()
        var collectedAttchments = [String: String]()
        var documentAttachments = Set<URL>()

        documentDictionary["documentId"] = document.documentID
        //aggregate attchments
        for revision in history {
            guard let props = revision.properties else { break }
            allRevisions.append(props)

            guard let attachments = revision.attachments else {
                continue
            }
            for attachment in attachments {
                guard
                    let revId = revision.revisionID,
                    let attachmentUrl = attachment.contentURL
                    else {
                        continue
                }
                collectedAttchments["\(revId)-\(attachment.name)"] = attachmentUrl.lastPathComponent
                documentAttachments.insert(attachmentUrl)
            }

        }
        if !collectedAttchments.isEmpty {
            documentDictionary["attachments"] = collectedAttchments
        }
        documentDictionary["revisions"] = allRevisions

        return (documentDictionary, documentAttachments)
    }

    // MARK: - Unarchiving
    func unarchiveProject(from fileUrl: URL, documentServer: DocumentServer) throws -> Project {
        let manager = try documentServer.createManager()

        //open zip archive
        guard let archive = Archive(url: fileUrl, accessMode: .read) else {
            throw ArchiveError.fatalError
        }

        //check version
        let indexData = try archive.extract(path: "index.json")
        guard
            let indexJson = try JSONSerialization.jsonObject(with: indexData, options: []) as? JSONDict,
            (indexJson["version"] as? Int) ?? -1 == 1
        else {
            throw ArchiveError.unsupportedVersion
        }

        //generate new project (with a fresh UUID)
        let projectUuid = UUID()
        let projectDocumentId = "MPProject:\(projectUuid.uuidString)"

        let project = try loadProject(from: archive, documentServer: documentServer, documentId: projectDocumentId)

        //new project collection database name
        let databaseName = (project.url.lastPathComponent as NSString).deletingPathExtension

        guard let database = try? manager.databaseNamed(databaseName) else {
            throw ArchiveError.fatalError
        }

        try loadDocuments(from: archive, into: database, containerId: projectDocumentId)

        return project
    }

    private func loadProject(from archive: Archive, documentServer: DocumentServer, documentId: String) throws -> Project {
        var userProfile: CBLDocument!
        documentServer.manager.doSync {
            userProfile = try? documentServer.manager.fetchUserProfile()
        }
        //should never happen, just to keep error handling nice
        guard
            userProfile != nil,
            let userId = userProfile.properties?["userID"] as? String
        else {
            throw ArchiveError.fatalError
        }

        let userDatabase = try documentServer.manager.databaseNamed("user")
        guard let projectDocument = userDatabase.document(withID: documentId) else {
            throw ArchiveError.fatalError
        }

        let projectData = try archive.extract(path: "project.json")
        guard
            let projectJson = try JSONSerialization.jsonObject(with: projectData, options: []) as? [JSONDict],
            let revisions = projectJson.first?["revisions"] as? [JSONDict],
            revisions.count == 1
        else {
            throw ArchiveError.invalidFormat
        }

        var revHistory = [String]()
        for var revisionJson in revisions {
            //fixup
            revisionJson["owners"] = [userId]
            revisionJson["_id"] = documentId
            //store
            guard let revId = revisionJson["_rev"] as? String else {
                throw ArchiveError.invalidFormat
            }
            revHistory.insert(revId, at: 0)
            try projectDocument.putExistingRevision(withProperties: revisionJson, attachments: nil, revisionHistory: revHistory, from: nil)
        }

        guard let project = Project(documentId: documentId, dict: projectDocument.properties!, enclave: documentServer.enclave!) else {
            throw ArchiveError.fatalError
        }

        return project
    }

    private func loadDocuments(from archive: Archive, into database: CBLDatabase, containerId: String) throws {
        let contentData = try archive.extract(path: "content.json")
        guard let contentJson = try JSONSerialization.jsonObject(with: contentData, options: []) as? [JSONDict] else {
            throw ArchiveError.invalidFormat
        }

        try database.inTransactionSync {
            for documentJson in contentJson {
                guard
                    let documentId = documentJson["documentId"] as? String,
                    let revisions = documentJson["revisions"] as? [JSONDict]
                    else {
                        throw ArchiveError.invalidFormat
                }
                let attachmentMappings = documentJson["attachments"] as? JSONDict

                guard let document = database.document(withID: documentId) else {
                    throw ArchiveError.fatalError
                }

                var revHistory = [String]()
                for var revisionJson in revisions {
                    revisionJson["containerID"] = containerId
                    guard let revId = revisionJson["_rev"] as? String else {
                        throw ArchiveError.invalidFormat
                    }
                    revHistory.insert(revId, at: 0)

                    //collect attachments
                    var attachmentBodies = [String: Data]()
                    if let attachmentsJson = revisionJson["_attachments"] as? JSONDict {
                        for attachmentName in attachmentsJson.keys {
                            guard
                                let bodyFileName = attachmentMappings?["\(revId)-\(attachmentName)"] as? String
                                else {
                                    throw ArchiveError.invalidFormat
                            }
                            let attachmentBody = try archive.extract(path: "attachments/\(bodyFileName)")
                            attachmentBodies[attachmentName] = attachmentBody
                        }
                    }

                    try document.putExistingRevision(withProperties: revisionJson, attachments: attachmentBodies, revisionHistory: revHistory, from: nil)
                }

            }
            return true
        }
    }

}
