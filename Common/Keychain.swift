//
//  Keychain.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Security

struct Keychain {

    enum KeychainError: Error, LocalizedError {
        case systemError(OSStatus)
        case itemExists
        case invalidInput
        case invalidKeychainData

        var errorDescription: String? {
            switch self {
            case .itemExists:
                return "Can not override existing item"
            case .invalidInput:
                return "Invalid input"
            case .invalidKeychainData:
                return "Invalid keychain data"
            case let .systemError(status):
                return (SecCopyErrorMessageString(status, nil) as String?) ?? "Unknown system error"
            }
        }
    }

    private enum QueryType {
        case query
        case fetch
        case add
    }

    private static func passwordQuery(account: String, service: String, type: QueryType) -> [String: Any] {
        var ret = [String: Any]()
        ret[kSecClass as String] = kSecClassGenericPassword
        ret[kSecAttrAccount as String] = account
        ret[kSecAttrService as String] = service

        switch type {
        case .query:
            break
        case .fetch:
            ret[kSecReturnData as String] = true
            ret[kSecReturnAttributes as String] = true
            ret[kSecMatchLimit as String] = kSecMatchLimitOne
        case .add:
            ret[kSecAttrAccessible as String] = kSecAttrAccessibleWhenUnlocked
        }
        return ret
    }

    private static func fetchItems(query: [String: Any]) throws -> [[String: Any]] {
        var outResult: AnyObject?
        let status = SecItemCopyMatching(query as CFDictionary, &outResult)
        guard status == errSecSuccess || status == errSecItemNotFound else {
            throw KeychainError.systemError(status)
        }

        var ret = [[String: Any]]()

        if status == errSecSuccess {
            guard let result = outResult else {
                throw KeychainError.invalidKeychainData
            }
            switch result {
            case let arr as [[String: Any]]:
                ret += arr
            case let dict as [String: Any]:
                ret.append(dict)
            default:
                throw KeychainError.invalidKeychainData
            }
        }
        return ret
    }

    static func storePasswordData(_ password: Data, account: String, service: String, replaceExisting: Bool = true) throws {
        let query = passwordQuery(account: account, service: service, type: .fetch)
        let items = try fetchItems(query: query)

        assert(items.count <= 1, "expected zero or one keychain item")

        if items.isEmpty {
            //add
            var attributes = passwordQuery(account: account, service: service, type: .add)
            attributes[kSecValueData as String] = password

            let status = SecItemAdd(attributes as CFDictionary, nil)
            guard status == errSecSuccess else {
                throw KeychainError.systemError(status)
            }
        } else {
            //update
            guard replaceExisting == true else {
                throw KeychainError.itemExists
            }

            var attributes = [String: Any]()
            attributes[kSecValueData as String] = password

            let updateQuery = passwordQuery(account: account, service: service, type: .query)
            let status = SecItemUpdate(updateQuery as CFDictionary, attributes as CFDictionary)
            guard status == errSecSuccess else {
                throw KeychainError.systemError(status)
            }
        }
    }

    static func storePasswordArchived(_ coding: Any, account: String, service: String, replaceExisting: Bool = true) throws {
        let data = try NSKeyedArchiver.archivedData(withRootObject: coding, requiringSecureCoding: true)
        try storePasswordData(data, account: account, service: service)
    }

    static func storePassword(_ password: String, account: String, service: String, replaceExisting: Bool = true) throws {
        guard let data = password.data(using: .utf8) else {
            throw KeychainError.invalidInput
        }
        try storePasswordData(data, account: account, service: service, replaceExisting: replaceExisting)
    }

    static func deletePassword(account: String, service: String) throws {
        let query = passwordQuery(account: account, service: service, type: .query)
        let status = SecItemDelete(query as CFDictionary)
        guard status == errSecSuccess || status == errSecItemNotFound else {
            throw KeychainError.systemError(status)
        }
    }

    static func getPasswordData(account: String, service: String) throws -> Data? {
        let query = passwordQuery(account: account, service: service, type: .fetch)
        if let item = try fetchItems(query: query).first {
            return item[kSecValueData as String] as? Data
        } else {
            return nil
        }
    }

    static func getPassword(account: String, service: String) throws -> String? {
        guard let data =  try getPasswordData(account: account, service: service) else {
            return nil
        }
        guard let password = String(data: data, encoding: .utf8) else {
            throw KeychainError.invalidKeychainData
        }
        return password
    }

    static func getPasswordUnarchived(account: String, service: String) throws -> Any? {
        guard let data = try getPasswordData(account: account, service: service) else {
            return nil
        }
        return try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data)
    }
}
