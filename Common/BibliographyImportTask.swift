//
//  BibliographyImportInteractiveTask.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Combine

/**
 Concrete implementation of an InteractiveTask.
 Uses a BibliographyImportProvider and transforms its results to form used by BibliographyImportView (sheet).
 */
class BibliographyImportTask: InteractiveTask {

    /// Published state used by SwiftUI
    struct ImportState {
        var description = "Importing Bibliography"
        var conclusion: String?
        var details: String?
        var lastError: LocalizedError?
        var buttons: [InteractiveButtonDefinition]?

        mutating func reset() {
            description = "Importing Bibliography"
            conclusion = nil
            lastError = nil
            details = nil
            buttons = nil
        }
    }

    typealias BibliographyImportProviderProtocol = BibliographyImport & InteractiveTaskProvider
    let importProvider: BibliographyImportProviderProtocol
    let nativeBridge: NativeBridge

    /// We have it all in a single property so we trigger only a single SwiftUI update
    @Published var importState = ImportState()

    init(importProvider: BibliographyImportProviderProtocol, nativeBridge: NativeBridge) {
        self.importProvider = importProvider
        self.nativeBridge = nativeBridge
        super.init(interactiveTaskProvider: importProvider)
    }

    var importSink: AnyCancellable?

    private var safeMode = false

    /// Update state atomically to reduce a number of UI updates
    private func editImportState(block: (inout ImportState) -> Void) {
        var auxState = importState
        block(&auxState)
        importState = auxState
    }

    /// Starts the import process.
    override func start() {
        super.start()
        self.importState.reset()

        progress.totalUnitCount = 1000
        let importProgress = Progress(totalUnitCount: 1000, parent: progress, pendingUnitCount: 900)
        let processingProgress = Progress(totalUnitCount: 1000, parent: progress, pendingUnitCount: 100)

        var importOptions = BibliographyImportOptions()
        if self.safeMode { importOptions.insert(.safeMode) }
        importSink = importProvider.importLibraryPublisher(progress: importProgress, options: importOptions)
            .sink(receiveCompletion: { [weak self] completion in
                if case let .failure(error) = completion {
                    self?.updateState(with: error)
                }
                self?.importSink = nil
            }, receiveValue: { result in
                self.processImportResult(result, progress: processingProgress)
            })
    }

    override func cancel() {
        importSink?.cancel()
        super.cancel()
    }

    private func retrySafely() {
        safeMode = true
        start()
    }

    private func updateState(with error: BibliographyImportError) {
        editImportState { editedState in
            editedState.lastError = error
            editedState.details = importProvider.describeError(error, detailed: false)
            if importProvider.canSafelyRetry(error: error) {
            editedState.conclusion = "An error occured but we may process results pedantically."
                editedState.buttons = [
                    InteractiveButtonDefinition(tag: 1, label: "Retry") { [weak self] in
                        self?.retrySafely()
                    }
                ]
                pause()
            } else {
                finish()
            }
        }
    }

    private func processImportResult(_ result: BibliogaphyImportResult, progress: Progress?, partialImport: Bool = false) {
        if !partialImport && !result.importErrors.isEmpty {
            editImportState { editedSate in
                editedSate.conclusion = "Imported sucesfully \(result.importedItems.count) items, \(result.importErrors.count) items failed. Please review details and click the Import button if you want to import partial results anyway."
                editedSate.details = result.importErrors
                    .compactMap({importProvider.describeError($0, detailed: true)})
                    .joined(separator: "\n")
                editedSate.buttons = [
                    InteractiveButtonDefinition(tag: 1, label: "Import partial results") { [weak self] in
                        self?.importState.reset()
                        self?.processImportResult(result, progress: progress, partialImport: true)
                    }
                ]
            }
            pause()
        } else {
            // TODO: enable the bulk mode when the frontend is ready issue
            // https://gitlab.com/mpapp-public/manuscripts-cocoa-app/-/issues/77
            importProvider.applyImportResult(result, nativeBridge: nativeBridge, progress: progress, options: [.preferBulkMode]) { result in
                self.concludeApplyResult(result)
                self.finish()
            }
            editImportState { editedState in
                editedState.reset()
            }
            resume()
        }
    }

    private func concludeApplyResult(_ result: Result<[NativeBridgeResult], BibliographyImportError>) {
        do {
            let results = try result.get()

            let errors = results.compactMap { result -> Error? in
                if case let .failure(error) = result { return error }
                return nil
            }

            // get imported count
            let imported = results.reduce(0, {
                let result = try? $1.get()
                let message = result as? [String: Any]
                let imported = (message?["imported"] as? Int) ?? 0
                return $0 + imported
            })

            if errors.isEmpty {
                editImportState { editedState in
                    editedState.buttons = []
                    editedState.conclusion = "Successfully imported \(imported) items"
                }
            } else {
                editImportState { editedState in
                    editedState.buttons = []
                    editedState.conclusion = "Imported \(imported) items with errors"
                    editedState.details = errors
                        .compactMap({importProvider.describeError($0, detailed: true)})
                        .joined(separator: "\n")
                }
            }
        } catch let err {
            editImportState { editedState in
                editedState.buttons = []
                editedState.conclusion = "Import failed"
                editedState.lastError = err as? LocalizedError
            }
        }
    }

}
