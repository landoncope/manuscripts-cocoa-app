//
//  ProjectList.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import CouchbaseLite
import Combine
import os.log

class ProjectList: ObservableObject {

    private(set) weak var enclave: DataEnclave?
    var database: CBLDatabase?

    @Published private(set) var projects = [String: Project]()

    private var query: CBLLiveQuery?
    private var rowsObserver: AnyCancellable?

    /// Maintained on the CBL manager queue exclusively and published via `projects` on the main thread.
    private var projectsCache = [String: Project]()

    init(enclave: DataEnclave) {
        self.enclave = enclave
    }

    /// Starts the query asyncronously
    func start() {
        guard
            let actualEnclave = enclave,
            let server = actualEnclave.documentServer
        else {
            preconditionFailure("project list not fully set")
        }

        server.assertManagerQueue()
        stop()

        let predicate = NSPredicate(format: "objectType = %@", "MPProject")
        guard let builder = try? CBLQueryBuilder(database: database, select: nil, wherePredicate: predicate, orderBy: nil) else {
            preconditionFailure("could not create live query as expected")
        }
        query = builder.createQuery(withContext: nil).asLive()

        rowsObserver = query?.publisher(for: \.rows)
            .sink { [weak self] queryRows in
                guard let this = self else { return }
                server.assertManagerQueue()

                var nextCache = [String: Project]()
                var projectsToUpdate = [(Project, [String: Any])]()
                queryRows?.forEach { row in
                    guard
                        let projectRow = row as? CBLQueryRow,
                        let revision = projectRow.document?.currentRevision,
                        let documentId = projectRow.document?.documentID,
                        let properties = revision.properties
                        else {
                            return
                    }
                    if let existingProject = this.projectsCache[documentId] {
                        projectsToUpdate.append((existingProject, properties))
                        nextCache[documentId] = existingProject
                    } else {
                        let project = Project(documentId: documentId, dict: properties, enclave: actualEnclave)
                        nextCache[documentId] = project
                    }
                }
                this.projectsCache = nextCache
                //update and publish a captured array on the main thread
                DispatchQueue.main.async { [capturedProjects = this.projectsCache] in
                    for (project, properties) in projectsToUpdate {
                        project.update(from: properties)
                    }
                    this.projects = capturedProjects
                }
            }
    }

    func stop() {
        guard let server = enclave?.documentServer else {
            return
        }
        server.assertManagerQueue()
        query?.stop()
        query = nil
        rowsObserver = nil
        DispatchQueue.main.async {
            self.projects = [:]
        }
    }

}
