//
//  InteractiveTask.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Combine
import SwiftUI

/// Protocol defining a provider for an interactive task.
protocol InteractiveTaskProvider {
    var sheetView: AnyView { get }

    func canSafelyRetry(error: Error) -> Bool
    func describeError(_ error: Error, detailed: Bool) -> String?
}

extension InteractiveTaskProvider {
    func canSafelyRetry(error: Error) -> Bool { false }
    func describeError(_ error: Error, detailed: Bool) -> String? { nil }
}

/// Definition of extra buttons inserted to the task sheet.
struct InteractiveButtonDefinition: Identifiable {
    let tag: Int
    let label: String
    let action: () -> Void

    // swiftlint:disable identifier_name
    var id: Int { tag }
}

/// This is a base class serving as a bridge between a task provider and SwiftUI interface.
class InteractiveTask: ObservableObject, Identifiable {

    enum TaskState {
        case inactive
        case running
        case paused
        case finished
        case cancelled
    }

    @Published var state = TaskState.inactive
    @Published var fractionCompleted: Double = 0.0

    private let interactiveTaskProvider: InteractiveTaskProvider

    init(interactiveTaskProvider: InteractiveTaskProvider) {
        self.interactiveTaskProvider = interactiveTaskProvider
    }

    var progress = Progress()

    var sheetView: AnyView {
        interactiveTaskProvider.sheetView
    }

    func start() {
        resetProgress()
        state = .running
    }

    func cancel() {
        state = .cancelled
    }

    func pause() {
        state = .paused
    }

    func resume() {
        state = .running
    }

    func finish() {
        state = .finished
    }

    private func resetProgress() {
        progress = Progress()
        progressSink?.cancel()
        progressSink = watchProgress()
    }

    private var progressSink: AnyCancellable?
    func watchProgress() -> AnyCancellable {
        return progress.publisher(for: \.fractionCompleted)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] value in
                self?.fractionCompleted = value
            }
    }
}
