//
//  SessionManager.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Combine
import SwiftUI
import os.log

class SessionManager: ObservableObject {

    private let urlSession = URLSession(configuration: URLSessionConfiguration.default)

    @Published
    private(set) var cloudSessionState: SessionState {
        willSet {
            dispatchPrecondition(condition: .onQueue(DispatchQueue.main))
            objectWillChange.send()
        }
        didSet {
            Self.storeSession(cloudSessionState)
        }
    }

    @Published
    private(set) var localSessionState: SessionState {
        willSet {
            dispatchPrecondition(condition: .onQueue(DispatchQueue.main))
            objectWillChange.send()
        }
    }

    @Published
    private(set) var standaloneSessionState: SessionState {
        willSet {
            dispatchPrecondition(condition: .onQueue(DispatchQueue.main))
            objectWillChange.send()
        }
    }

    @Published
    private(set) var credentialState = CredentialEntryState.idle {
        willSet {
            dispatchPrecondition(condition: .onQueue(DispatchQueue.main))
            objectWillChange.send()
        }
        didSet {
            proceedLogin()
        }
    }

    /**
     This property is set if the authentication procss resulted to user account switch
     and we need some user interaction to confirm the current local data would be purged.
     */
    @Published
    private(set) var sessionSwitchRequest: SessionState?

    init() {
        cloudSessionState = Self.restoreSession() ?? SessionState.void
        localSessionState = SessionState.void
        standaloneSessionState = SessionState.void
    }

    var currentUsername: String? {
        cloudSessionState.user?.username
    }

    // MARK: - Login
    func submitCredentials(_ credentials: Credentials) {
        guard case .requested = credentialState else {
            return
        }
        credentialState = .submitted(credentials)
    }

    func requestLogin(message: String? = nil) {
        switch credentialState {
        case .idle, .requested:
            credentialState = .requested(message)
        default:
            break
        }
    }

    func cancelLogin() {
        credentialState = .idle
    }

    // TODO: Move this to a developer/test zone
    private func proceedLogin() {
        guard case let .submitted(credentials) = credentialState else {
            return
        }

        let request: URLRequest
        do {
            request = try LoginRequest.request(with: credentials)
        } catch let error {
            os_log(.error, "could not create login request: %{public}@", error.localizedDescription)
            assertionFailure("could not create login request: \(error.localizedDescription)")
            return
        }

        _ = self.urlSession.dataTaskPublisher(for: request)
            .receive(on: DispatchQueue.main)
            .tryMap(responseToSessionState(for: request, credentials: credentials))
            .sink(receiveCompletion: { (completion) in
                //on error we do not change the login state, we only request credentials again
                if case let .failure(error) = completion {
                    self.credentialState = .requested(error.localizedDescription)
                }
            }, receiveValue: { state in
                self.cloudSessionState = state
                self.credentialState = .idle
            })
    }

    // TODO: Move this to a developer/test zone
    private func responseToSessionState(for request: URLRequest, credentials: Credentials) -> ((_ data: Data, _ response: URLResponse) throws -> SessionState) {
        return { (data, response) in
            guard let httpResponse = response as? HTTPURLResponse else {
                throw APIError.invalidServerResponse
            }
            switch httpResponse.statusCode {
            case 200:
                let loginResponse = try JSONDecoder().decode(LoginResponse.self, from: data)
                var cookies = [HTTPCookie]()
                if let typedCookies = httpResponse.allHeaderFields as? [String: String] {
                    cookies = HTTPCookie.cookies(withResponseHeaderFields: typedCookies, for: request.url!)
                }
                let syncGatewayCookies = try SessionState.syncGatewayCookies(from: cookies)
                let state = try SessionState(token: loginResponse.token, gatewayCookies: syncGatewayCookies)
                return state
            default:
                let errorResponse = try JSONDecoder().decode(ServerError.self, from: data)
                throw APIError.serverError(errorResponse)
            }
        }
    }

    /**
     Accepts a new token and cookies and returns status of this operation.

     If the new token refers to an another account then the current one this function returns false, unless `allowAccountSwitch` was set to `true`. In such case new `SessionState` is published via `sessionSwitchRequest` supposedly subscribed by code handling UI cleanup before the account is switched.

     After UI cleanup `sessionSwitchRequest` may be accepted by calling `acceptSessionSwitchRequest`.

     - Parameters:
         - token: New token
         - cookies: New SG cookies
         - allowAccountSwitch: If set then the new `SessionInfo is published via `sessionSwitchRequest` before it's applied.

     - Returns: Boolean value indicating the tokne/cookies were accepted in any way.
     */
    func acceptLoginToken(_ token: String, cookies: [String: HTTPCookie]?, allowAccountSwitch: Bool = false) throws -> Bool {
        var success = false

        let nextSessionState = try SessionState(token: token, gatewayCookies: cookies)
        if cloudSessionState.user == nil || cloudSessionState.user == nextSessionState.user {
            self.cloudSessionState = nextSessionState
            success = true
        } else if allowAccountSwitch {
            sessionSwitchRequest = nextSessionState
            success = true
        }
        if success {
            credentialState = .idle
        }
        return success
    }

    func resetSessionSwitchRequest() {
        sessionSwitchRequest = nil
    }

    func acceptSessionSwitchRequest() {
        guard let requestedState = sessionSwitchRequest else {
            return
        }
        cloudSessionState = requestedState
        resetSessionSwitchRequest()
    }

    // MARK: - Logout
    private func performLogOut() {
        self.cloudSessionState = SessionState.void
    }

    func logout(reason: LogoutReason, userConfirmed: Bool = true) {
        switch reason {
        case .accountSwitch:
            if userConfirmed {
                acceptSessionSwitchRequest()
            } else {
                resetSessionSwitchRequest()
            }
        case .userInitiated:
            if userConfirmed {
                performLogOut()
            }
        }
    }

    // MARK: - Cookie Management

    private var refreshCookiesSink: AnyCancellable?

    func refreshSyncGatewayCookies() {
        dispatchPrecondition(condition: DispatchPredicate.onQueue(.main))

        //ignore parallel requests
        guard refreshCookiesSink == nil else { return }

        guard let token = cloudSessionState.token else {
            return
        }

        //forget cookies for a proper session state evaluation
        cloudSessionState = cloudSessionState.resettingCookies()

        let request: URLRequest
        do {
            request = try RefreshSyncSessionRequest.request(with: token)
        } catch let error {
            os_log(.error, "failed to create refresh gateway cookies request: %{public}@", error.localizedDescription)
            assertionFailure("failed to create refresh gateway cookies request: \(error.localizedDescription)")
            return
        }

        refreshCookiesSink = self.urlSession.dataTaskPublisher(for: request)
            .tryMap(responseToSyncCookies(for: request))
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { (completion) in
                if case let .failure(error) = completion {
                    switch error {
                    case APIError.authenticationRequired:
                        //forget token
                        self.cloudSessionState = self.cloudSessionState.resettingToken()
                    default:
                        os_log(.error, "error refreshing cookies: %{public}@", error.localizedDescription)
                    }
                }
                self.refreshCookiesSink = nil
            }, receiveValue: { (cookies) in
                guard let newCookies = cookies, self.cloudSessionState.token != nil else {
                    return
                }
                self.cloudSessionState = self.cloudSessionState.resettingCookies(newCookies)
            })
    }

    private func responseToSyncCookies(for request: URLRequest) -> ((_ data: Data, _ response: URLResponse) throws -> GatewayCookies?) {
        return { (data, response) in
            guard let httpResponse = response as? HTTPURLResponse else {
                throw APIError.invalidServerResponse
            }
            switch httpResponse.statusCode {
            case 200..<300:
                var cookies = [HTTPCookie]()
                if let typedCookies = httpResponse.allHeaderFields as? [String: String] {
                    cookies = HTTPCookie.cookies(withResponseHeaderFields: typedCookies, for: request.url!)
                }
                return try SessionState.syncGatewayCookies(from: cookies)
            case 401:
                throw APIError.authenticationRequired
            default:
                let errorResponse = try JSONDecoder().decode(ServerError.self, from: data)
                throw APIError.serverError(errorResponse)
            }
        }
    }

    // MARK: - Session Access
    subscript(scope: DataEnclave.Scope) -> SessionState {
        switch scope {
        case .local:
            return localSessionState
        case .standalone:
            return standaloneSessionState
        case .cloud:
            return cloudSessionState
        }
    }

    func sessionStatePublisher(for scope: DataEnclave.Scope) -> Published<SessionState>.Publisher {
        switch scope {
        case .local:
            return $localSessionState
        case .standalone:
            return $standaloneSessionState
        case .cloud:
            return $cloudSessionState
        }
    }

    func setSession(state: SessionState, in scope: DataEnclave.Scope) {
        switch scope {
        case .local:
            localSessionState = state
        case .standalone:
            standaloneSessionState = state
        case .cloud:
            preconditionFailure("cloud scoped session state must not be set directly")
            //cloudSessionState = state
        }
    }

    // MARK: - Session Persistence

    static let keychainService = "SessionState"
    static let keychainAccount = ".current"

    private class func storeSession(_ sessionState: SessionState) {
        guard sessionState != SessionState.void else {
            resetStoredSession()
            return
        }
        do {
            guard let sessionDict = sessionState.dictionary else {
                return
            }
            try Keychain.storePasswordArchived(sessionDict, account: Self.keychainAccount, service: Self.keychainService)
        } catch let error {
            //we consider this is a development phase error
            os_log(.error, "could not serialize session: %{public}@", error.localizedDescription)
            assertionFailure()
        }
    }

    private class func restoreSession() -> SessionState? {
        do {
            guard let sessionDict = try Keychain.getPasswordUnarchived(account: Self.keychainAccount, service: Self.keychainService) as? [String: Any] else {
                return nil
            }
            return SessionState(dictionary: sessionDict)
        } catch let error {
            os_log(.error, "could not restore session from keychain: %{public}@", error.localizedDescription)
            assertionFailure()
        }
        return nil
    }

    private class func resetStoredSession() {
        do {
            try Keychain.deletePassword(account: Self.keychainAccount, service: Self.keychainService)
        } catch let error {
            os_log(.error, "could not remove session keychain data: %{public}@", error.localizedDescription)
            assertionFailure()
        }
    }

}
