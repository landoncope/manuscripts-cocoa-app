//
//  WebKitDataManager.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import os.log

struct WebKitDataManager {

    let url: URL

    enum Error: LocalizedError {
        case invalidUrl
    }

    enum DataType: CaseIterable {
        case localDB
        case indexedDB

        static let allTypes = Set(DataType.allCases)
    }

    /// Maps the `url` to known convention used by the WebKit.
    private func baseName() throws -> String {
        guard let scheme = url.scheme, let host = url.host else {
            throw Error.invalidUrl
        }
        return "\(scheme)_\(host)"
    }

    private func websiteDataUrl() throws -> URL {
        let libraryUrl = try FileManager.default.url(for: .libraryDirectory, in: .userDomainMask, appropriateFor: nil, create: false)

        var subpath = "WebKit"
        if !Sandbox.isSandboxed { subpath += "/" + (Bundle.main.bundleIdentifier ?? "_") }
        subpath += "/WebsiteData"

        return libraryUrl.appendingPathComponent(subpath)
    }

    private func localDBResources() throws -> [URL] {
        let localDBUrl = try websiteDataUrl()
            .appendingPathComponent("LocalStorage")
        if FileManager.default.fileExists(atPath: localDBUrl.path) {
            let candidates = try FileManager.default.contentsOfDirectory(at: localDBUrl, includingPropertiesForKeys: nil, options: [])
            let prefix = try baseName() + "_0"
            return candidates.filter({$0.lastPathComponent.hasPrefix(prefix)})
        }
        return []
    }

    private func indexedDBResources() throws -> [URL] {
        let baseFileName = try baseName()
        let indexDBUrl = try websiteDataUrl()
            .appendingPathComponent("IndexedDB/v1")
            .appendingPathComponent("\(baseFileName)_0")
        return FileManager.default.fileExists(atPath: indexDBUrl.path) ? [indexDBUrl] : []
    }

    private func resourceURLsOfTypes(_ types: Set<DataType>) throws -> [URL] {
        #if os(iOS)
        #warning("check this is valid for iOS too")
        #endif
        var collected = [URL]()
        for type in types {
            switch type {
            case .localDB:
                collected += try localDBResources()
            case .indexedDB:
                collected += try indexedDBResources()
            }
        }
        return collected
    }

    func reset(types: Set<DataType> = DataType.allTypes) throws {
        let resourceUrls = try resourceURLsOfTypes(types)
        for resourceUrl in resourceUrls {
            try FileManager.default.removeItem(at: resourceUrl)
        }
    }

}
