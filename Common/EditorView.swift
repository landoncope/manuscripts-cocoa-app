//
//  EditorView.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import SwiftUI
import Combine

/**
 Observable class defining content and scope of an editor window. Passed as an `.environmentObject`.
 */
class EditorContent: ObservableObject {

    enum Action {
        case noop
        case newProject
        case openProject
    }

    let enclave: DataEnclave
    let action: Action

    @Published private(set) var project: Project?

    init(enclave: DataEnclave, action: Action = .noop) {
        self.enclave = enclave
        self.project = nil
        self.action = action
    }

    init(project: Project) {
        self.enclave = project.enclave
        self.project = project
        self.action = .openProject
    }

    func takeProject(_ project: Project) {
        assert(action == .newProject && self.project == nil && project.enclave === enclave)
        self.project = project
    }

    var documentType: String {
        return enclave.scope == .standalone ? DocumentType.standalone.rawValue : DocumentType.managed.rawValue
    }

}

struct EditorView: View {

    @EnvironmentObject var content: EditorContent
    @EnvironmentObject var toolbarState: ToolbarState
    @EnvironmentObject var mainMenuState: MainMenuState
    @EnvironmentObject var interactiveContext: InteractiveContext

    @State var webViewLoaded = false

    var body: some View {
        VStack {
            #if os(iOS)
            ToolbarView()
                .frame(height: 50)
            EditorWebView(project: nil, webViewLoaded: $webViewLoaded)
            #else
            EditorWebViewMac(webViewLoaded: $webViewLoaded)
            #endif
        }
        .sheet(item: $interactiveContext.interactiveTask) { task in
            task.sheetView
                .environmentObject(self.interactiveContext)
                .environmentObject(task)
        }
    }
}

#if DEBUG
struct EditorViewPreviews: PreviewProvider {
    static var previews: some View {
        EditorView()
            .environmentObject(EditorContent(enclave: AppEnvironment.shared.dataManager[.local]))
            .environmentObject(ToolbarState())
            .environmentObject(MainMenuState())
            .environmentObject(InteractiveContext())
    }
}
#endif
