//
//  LoginTypes.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import os.log

struct Credentials: Equatable {
    let username: String
    let password: String

    init (username: String, password: String) {
        self.username = username
        self.password = password
    }
}

enum LogoutReason {
    case userInitiated
    case accountSwitch
}

// MARK: -
struct LoginRequest: Encodable {

    private enum CodingKeys: String, CodingKey {
        case username = "email"
        case password
        case deviceId
    }

    let username: String
    let password: String
    let deviceId: UUID

    init(credentials: Credentials, deviceId: UUID) {
        username = credentials.username
        password = credentials.password
        self.deviceId = deviceId
    }

    static let loginUrl = URL(string: Configuration.standard.apiBaseUrl)!.appendingPathComponent("auth/login")

    static func request(with credentials: Credentials) throws -> URLRequest {
        var request = URLRequest(url: loginUrl)

        let payload = LoginRequest(credentials: credentials, deviceId: DeviceUUID.uuid)
        try request.setPayload(json: payload)

        //special headers for login
        guard let bundleIdentifier = Bundle.main.bundleIdentifier else {
            throw APIError.missingBundleIdentifier
        }
        request.setValue(bundleIdentifier, forHTTPHeaderField: URLRequest.HTTPHeader.manuscriptsAppId)
        request.setValue(Configuration.standard.applicationSecret, forHTTPHeaderField: URLRequest.HTTPHeader.manuscriptsAppSecret)

        return request
    }
}

struct LoginResponse: Decodable {
    let token: String
}

// MARK: -
struct AuthIamRequest {

    let deviceId: UUID

    init(deviceId: UUID) {
        self.deviceId = deviceId
    }

    static let authUrl = URL(string: Configuration.standard.apiBaseUrl)!.appendingPathComponent("auth/iam")

    static func request() throws -> URLRequest {
        var comps = URLComponents(url: authUrl, resolvingAgainstBaseURL: true)!
        comps.queryItems = [
            URLQueryItem(name: "deviceId", value: DeviceUUID.uuid.uuidString)
        ]

        var request = URLRequest(url: comps.url!)

        //special headers for login
        guard let bundleIdentifier = Bundle.main.bundleIdentifier else {
            throw APIError.missingBundleIdentifier
        }
        request.setValue(bundleIdentifier, forHTTPHeaderField: URLRequest.HTTPHeader.manuscriptsAppId)
        request.setValue(Configuration.standard.applicationSecret, forHTTPHeaderField: URLRequest.HTTPHeader.manuscriptsAppSecret)

        return request
    }
}

// MARK: -
struct RefreshSyncSessionRequest: Encodable {
    //any value for the post request
    let value = true

    static let refreshUrl = URL(string: Configuration.standard.apiBaseUrl)!.appendingPathComponent("auth/refreshSyncSessions")

    static func request(with token: String) throws -> URLRequest {
        var request = URLRequest(url: refreshUrl)

        let payload = RefreshSyncSessionRequest()
        try request.setPayload(json: payload, token: token)

        return request
    }
}
