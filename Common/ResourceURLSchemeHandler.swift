//
//  ResourceURLSchemeHandler.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import WebKit
import os.log
import CoreServices

class ResourceURLSchemeHandler: NSObject, WKURLSchemeHandler {

    enum Error: Swift.Error {
        case noURL
        case unknownMIMEType(url: URL)
        case fileNotFound(url: URL)
    }

    enum PrefixMapping {
        case directory(String)
        case handler(WKURLSchemeHandler)
    }

    private var prefixMappings = [String: PrefixMapping]()

    func map(pathPrefix: String, to subdirectory: String) {
        prefixMappings[pathPrefix] = .directory(subdirectory)
    }

    func map(pathPrefix: String, to handler: WKURLSchemeHandler) {
        prefixMappings[pathPrefix] = .handler(handler)
    }

    func mapping(for url: URL) -> PrefixMapping? {
        guard url.pathComponents.count > 1, let mapping = prefixMappings[url.pathComponents[1]] else {
            return nil
        }
        return mapping
    }

    func webView(_ webView: WKWebView, start urlSchemeTask: WKURLSchemeTask) {
        #if LOG_SCHEME_HANDLER
        os_log(.debug, "ResourceURLSchemeHandler start task: %{public}@", urlSchemeTask.request.url!.absoluteString)
        #endif

        guard let requestedURL = urlSchemeTask.request.url else {
            urlSchemeTask.didFailWithError(Error.noURL)
            return
        }

        switch mapping(for: requestedURL) {
        case nil:
            self.respondToTask(urlSchemeTask)
        case let .directory(dir):
            self.respondToTask(urlSchemeTask, mappedDirectory: dir)
        case let .handler(handler):
            handler.webView(webView, start: urlSchemeTask)
        }
    }

    func webView(_ webView: WKWebView, stop urlSchemeTask: WKURLSchemeTask) {
        guard let requestedURL = urlSchemeTask.request.url else {
            urlSchemeTask.didFailWithError(Error.noURL)
            return
        }

        switch mapping(for: requestedURL) {
        case let .handler(handler):
            handler.webView(webView, stop: urlSchemeTask)
        default:
            os_log(.info, "webView(_:stop:) not implemented what may lead to crash soon.")
        }
    }

    private func mimeType(for url: URL) -> String {
        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, url.pathExtension as NSString, nil)?.takeRetainedValue(),
            let auxType = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
            return auxType as String
        } else {
            switch url.pathExtension {
            case "map":
                return "application/json"
            default:
                return "application/octet-stream"
            }
        }
    }

    /// Translates processed app-resource: URL to file URL pointing to a bundled file.
    private func translate(url: URL, mappedDirectory: String?) throws -> URL {
        let components = url.pathComponents
        //If there is some path (at least a trailing slash after the host name)
        //path components contain "/" as the first item.
        if let mappedDir = mappedDirectory {
            let resourceDirUrl = Bundle.main.resourceURL!.absoluteURL
                .appendingPathComponent(mappedDir)
                .appendingPathComponent("data")

            let potentialUrl: URL
            if components.count > 2 {
                potentialUrl = resourceDirUrl.appendingPathComponent(components[2...].joined(separator: "/"))
            } else {
                potentialUrl = resourceDirUrl
            }
            guard FileManager.default.fileExists(atPath: potentialUrl.path) else {
                assertionFailure("file not found: \(url)")
                throw Error.fileNotFound(url: url)
            }
            return potentialUrl
        } else {
            //TODO: review see #56
            //default processing for manuscripts-frontend
            let resourceDirUrl = Bundle.main.resourceURL!.absoluteURL
                .appendingPathComponent("manuscripts-frontend")
                .appendingPathComponent("dist")

            if url.path == "" || url.path == "/" || resourceDirUrl.path == "/login" {
                return resourceDirUrl.appendingPathComponent("index.html")
            } else {
                let potentialUrl = resourceDirUrl.appendingPathComponent(url.path)
                if FileManager.default.fileExists(atPath: potentialUrl.path) {
                    return potentialUrl
                } else {
                    return resourceDirUrl.appendingPathComponent("index.html")
                }
            }
        }
    }

    private func respondToTask(_ urlSchemeTask: WKURLSchemeTask, mappedDirectory: String? = nil) {
        do {
            let requestedUrl = urlSchemeTask.request.url!
            let resourceUrl = try translate(url: requestedUrl, mappedDirectory: mappedDirectory)
            let resourceContents = try Data(contentsOf: resourceUrl)

            let mimeTypeString = mimeType(for: resourceUrl)

            // WebKit XHR is failing if plain URLResponse is used.
            let response = HTTPURLResponse(url: requestedUrl, statusCode: 200, httpVersion: "1.0", headerFields: ["Content-type": mimeTypeString])!

            urlSchemeTask.didReceive(response)
            urlSchemeTask.didReceive(resourceContents)
            urlSchemeTask.didFinish()
        } catch {
            urlSchemeTask.didFailWithError(error)
            return
        }
    }
}
