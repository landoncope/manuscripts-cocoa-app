//
//  FilterProcess.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import os.log

/**
 Extrenal task based file filter. Can process the file either as whole or chunk by chunk.
 */
class FilterProcess {

    private class FilterState {
    }

    enum FilterError: Error {
        case fileNotFound(URL)
        case filterError(Data, Data) //chunk data, stderror data
        case processError(Error)
    }

    typealias LineFilter = (Data) -> Data
    typealias FileChunkResult = Result<Data, FilterError>// (data: Data?, error: FilterError?)

    let executableUrl: URL
    let arguments: [String]?
    let queue: DispatchQueue

    var linePreprocessor: LineFilter?
    var chunkRecogniser: FileChunks.ChunkRecogniser?

    var trimLineEnds = false
    var lineEnd = FileLines.LineEnd.unix

    init(executableUrl: URL, arguments: [String]?) {
        self.executableUrl = executableUrl
        self.arguments = arguments ?? []
        self.queue = DispatchQueue(label: "filter_queue.\(executableUrl.lastPathComponent)", qos: .userInitiated, attributes: [], autoreleaseFrequency: .inherit, target: DispatchQueue.global(qos: .userInitiated))
    }

    func process(fileUrl: URL, progress: Progress? = nil, completion: @escaping (FilterResult) -> Void) {
        queue.async {
            self.processGated(fileUrl: fileUrl, progress: progress, completion: completion)
        }
    }

    private typealias DataHandler = (Data) -> Void

    typealias FilterResult = Result<[FileChunkResult], FilterError>

    // swiftlint:disable function_body_length
    private func processGated(fileUrl: URL, progress: Progress?, completion: (FilterResult) -> Void) {
        guard let lines = FileLines(fileUrl: fileUrl, lineEnd: lineEnd, trimLineEnd: trimLineEnds) else {
            completion(.failure(FilterError.fileNotFound(fileUrl)))
            return
        }

        let fileSize = try? FileManager.default.attributesOfItem(atPath: fileUrl.path)[.size] as? Int64 ?? 0
        progress?.totalUnitCount = fileSize ?? 0

        var chunkResults = [FileChunkResult]()

        for chunk in FileChunks(fileLines: lines, chunkRecogniser: chunkRecogniser) {
            if isCancelled { break } //no completion, just silent

            let filter = Process()
            filter.qualityOfService = .userInitiated
            filter.executableURL = executableUrl
            filter.arguments = arguments

            let lineIterator = chunk.makeIterator()

            let filterInput = Pipe()
            filter.standardInput = filterInput

            //collected data for error reporting
            var chunkData = Data()

            filterInput.fileHandleForWriting.writeabilityHandler = { handle in
                guard let line = lineIterator.next() else {
                    try? handle.close()
                    return
                }
                chunkData.append(line)
                let preprocessedLine = self.linePreprocessor?(line) ?? line
                handle.write(preprocessedLine)

                if let processingProgress = progress {
                    DispatchQueue.main.async {
                        processingProgress.completedUnitCount = min(processingProgress.totalUnitCount, processingProgress.completedUnitCount + Int64(line.count))
                    }
                }
            }

            var outData = Data()
            let filterOutput = Pipe()
            filter.standardOutput = filterOutput
            filterOutput.fileHandleForReading.readabilityHandler = { handle in
                let data = handle.availableData
                outData.append(data)
            }

            var outError = Data()
            let errorOutput = Pipe()
            filter.standardError = errorOutput
            errorOutput.fileHandleForReading.readabilityHandler = { handle in
                let data = handle.availableData
                outError.append(data)
            }

            let semaphore = DispatchSemaphore(value: 0)
            filter.terminationHandler = { proc in
                semaphore.signal()
            }

            do {
                try filter.run()
            } catch let error {
                completion(.failure(FilterError.processError(error)))
                return
            }

            semaphore.wait()

            filterInput.fileHandleForWriting.writeabilityHandler = nil
            filterOutput.fileHandleForReading.readabilityHandler = nil
            errorOutput.fileHandleForReading.readabilityHandler = nil

            guard filter.terminationReason == .exit, filter.terminationStatus == 0 else {
                chunkResults.append(.failure(FilterError.filterError(chunkData, outError)))
                continue
            }

            chunkResults.append(.success(outData))
        }
        if !isCancelled {
            completion(.success(chunkResults))
        }
    }

    private var cancelled = false

    let cancellationLock = NSLock()

    func cancel() {
        cancellationLock.lock()
        defer { cancellationLock.unlock() }
        cancelled = true
    }

    var isCancelled: Bool {
        cancellationLock.lock()
        defer { cancellationLock.unlock() }
        return cancelled
    }

}
