//
//  AppDelegateMac.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Cocoa
import SwiftUI
import os.log
import Combine
import WebKit

@NSApplicationMain
@objc
class AppDelegateMac: NSObject, NSApplicationDelegate, AppEnvironmentProvider {

    @IBOutlet weak var servicesStatusMenuItem: NSMenuItem!
    @IBOutlet weak var userNameMenuItem: NSMenuItem!

    @IBOutlet weak var fileMenu: NSMenu!
    @IBOutlet weak var fileNewMenu: NSMenu!
    @IBOutlet weak var fileExportMenu: NSMenu!
    @IBOutlet weak var editMenu: NSMenu!
    @IBOutlet weak var insertMenu: NSMenu!
    @IBOutlet weak var formatMenu: NSMenu!
    @IBOutlet weak var formatTableMenu: NSMenu!

    @IBOutlet weak var developerMenu: NSMenuItem!

    @IBOutlet weak var localProjectsMenu: NSMenu!
    @IBOutlet weak var cloudProjectsMenu: NSMenu!

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        setupMainMenu()
        do {
            networkReachability = try startNetworkReachability()
            dataManager = try DataManager(sessionManager: sessionManager)
            credentialStateSink = watchCredentialState()
            sessionSwitchSink = watchSessionSwitchRequest()
            servicesStatusSink = watchServicesStatus()
            (localProjectsSink, cloudProjectsSink) = setupProjectLists()
        } catch let error {
            NSApp.presentError(error)
        }
        #if DEBUG
        print("sandbox dir: \(FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask).first!.path)")
        #endif
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }

    func applicationOpenUntitledFile(_ sender: NSApplication) -> Bool {
        //here we lie saying that we opened an untitled document, but we did not
        true
    }

    // MARK: - Logging In/Out
    let sessionManager: SessionManager! = SessionManager()

    private var credentialStateSink: AnyCancellable?

    private func watchCredentialState() -> AnyCancellable {
        return sessionManager.$credentialState
            .map { state -> Bool in
                return state != .idle
            }
            .removeDuplicates()
            .receive(on: DispatchQueue.main)
            .sink { [weak self] showLogin in
                self?.showLogin(showLogin)
            }
    }

    private var loginModalSession: NSApplication.ModalSession?

    private var loginWindow: NSWindow?

    private func showLogin(_ showLogin: Bool) {
        if showLogin && self.loginModalSession == nil {
            let credsView = ConnectLoginView()
                .environmentObject(ConnectLoginView.ConnectState(sessionManager: self.sessionManager))
            let hosting = NSHostingController(rootView: credsView)
            loginWindow = NSWindow(contentViewController: hosting)
            loginWindow?.styleMask = [.borderless, .titled, .closable]
            loginWindow?.title = "Welcome to Manuscripts"
            loginWindow?.delegate = self
            //we need a modal session to receive other events on the main queue
            self.loginModalSession = NSApp.beginModalSession(for: loginWindow!)
        } else if !showLogin, self.loginModalSession != nil {
            NSApp.abortModal()
            NSApp.modalWindow?.close()
            NSApp.endModalSession(self.loginModalSession!)
            loginModalSession = nil
            loginWindow = nil
        }
    }

    struct CloseDocumentsContext {
        private static var loggingOutContext = 1

        @RawContext(&Self.loggingOutContext) static var loggingOut
    }

    private var loggingOutReason: LogoutReason?

    /**
     Starts the log out process by attempting to close all documents.
     If this phase succeeds control is handed over to the SessionManager.
     */
    private func logOut(reason: LogoutReason) {
        guard loggingOutReason == nil else {
            assertionFailure("logout UI is not re-entrant")
            return
        }
        loggingOutReason = reason
        NSDocumentController.shared.closeAllDocuments(withDelegate: self, didCloseAllSelector: #selector(documentController(_:didCloseAll:contextInfo:)), contextInfo: CloseDocumentsContext.loggingOut)
    }

    @objc func documentController(_: NSDocumentController, didCloseAll: Bool, contextInfo: UnsafeMutableRawPointer?) {
        switch contextInfo {
        case CloseDocumentsContext.loggingOut:
            guard let reason = loggingOutReason else {
                assertionFailure("missing logout reason")
                return
            }
            // we need to let the release poool run once to deallocate edit window coordinators
            DispatchQueue.main.async {
                self.sessionManager.logout(reason: reason, userConfirmed: didCloseAll)
                self.loggingOutReason = nil
            }
        default:
            break
        }
    }

    private var sessionSwitchSink: AnyCancellable?

    private func watchSessionSwitchRequest() -> AnyCancellable {
        return sessionManager.$sessionSwitchRequest
            .removeDuplicates()
            //we are still in SessionManager.sessionSwitchRequest.willChange
            //we need to postpone next porcessing to be sure the login manager is fully updated
            .receive(on: DispatchQueue.main)
            .sink { (requestedState) in
                if requestedState != nil {
                    self.logOut(reason: LogoutReason.accountSwitch)
                }
            }
    }

    @IBAction func loginAction(_ sender: Any) {
        guard loggingOutReason == nil else {
            assertionFailure("can not log in while logging out")
            return
        }
        self.sessionManager.requestLogin()
    }

    @IBAction func logoutAction(_ sender: Any) {
        logOut(reason: .userInitiated)
    }

    // MARK: - Data
    private(set) var dataManager: DataManager!

    private var localProjectsSink: AnyCancellable?
    private var cloudProjectsSink: AnyCancellable?

    // MARK: - Services Status
    var networkReachability: NetworkReachability!

    private func startNetworkReachability() throws -> NetworkReachability {
        let reachability = try NetworkReachability.networkReachabilityForInternetConnection()
        reachability.start()
        return reachability
    }

    private var servicesStatusSink: AnyCancellable?

    func watchServicesStatus() -> AnyCancellable {
        guard let cloudDocumentServer = dataManager[.cloud].documentServer as? CloudDocumentServer else {
            preconditionFailure("unexpected server type")
        }
        return networkReachability.$status
            .combineLatest(sessionManager.$cloudSessionState, cloudDocumentServer.$replicationStatus)
            .removeDuplicates { $0.0 == $1.0 && $0.1 == $1.1 && $0.2 == $1.2 }
            .sink { (reachabilityStatus, sessionState, replicationStatus) in
                self.userNameMenuItem.title = "User: \(self.sessionManager.cloudSessionState.user?.username ?? "anonymous")"
                guard case .reachable = reachabilityStatus else {
                    self.servicesStatusMenuItem.title = "Offline: Not syncing"
                    self.servicesStatusMenuItem.isEnabled = false
                    return
                }
                switch replicationStatus {
                case .stopped:
                    self.servicesStatusMenuItem.title = "Online: Sync stopped"
                    self.servicesStatusMenuItem.isEnabled = false
                case .running:
                    self.servicesStatusMenuItem.title = "Online: Sync running"
                    self.servicesStatusMenuItem.isEnabled = false
                case .failed:
                    if sessionState.token == nil {
                        //we need to log in again
                        self.servicesStatusMenuItem.title = "Online: Needs login"
                        self.servicesStatusMenuItem.isEnabled = true
                        self.servicesStatusMenuItem.action = #selector(self.forceLogin(_:))
                    } else {
                        //some different error, we may try to restart
                        self.servicesStatusMenuItem.title = "Online: Retry sync"
                        self.servicesStatusMenuItem.isEnabled = true
                        self.servicesStatusMenuItem.action = #selector(self.retryReplication(_:))
                    }
                }
            }
    }

    @IBAction func forceLogin(_ sender: Any) {
        sessionManager.requestLogin()
    }

    @IBAction func retryReplication(_ sender: Any) {
        sessionManager.refreshSyncGatewayCookies()
    }

}

// MARK: - NSWindowDelegate

extension AppDelegateMac: NSWindowDelegate {

    func windowShouldClose(_ sender: NSWindow) -> Bool {
        if sender == loginWindow {
            sessionManager.cancelLogin()
            return false
        }
        return true
    }

}

// MARK: - Menu Delegate
extension AppDelegateMac: NSMenuDelegate, MenuUpdateProvider {

    func updateIdentifier(for menu: NSMenu) -> String? {
        switch menu {
        //case fileMenu: return "project"
        //case fileNewMenu: return "project-new"
        //case fileExportMenu: return "export"
        case editMenu: return "edit"
        case insertMenu: return "insert"
        case formatMenu: return "format"
        case formatTableMenu: return "format-table"
        default: return nil
        }
    }

    var watchedMenus: [NSMenu] {
        return [fileMenu, fileNewMenu, fileExportMenu, editMenu, insertMenu, formatMenu, formatTableMenu]
    }

    private func setupMainMenu() {
        #if !DEBUG
        NSApp.mainMenu?.removeItem(developerMenu)
        #endif

        for menu in watchedMenus {
            menu.delegate = self
            for item in menu.items {
                guard let menuItem = MainMenuItem.tagMapping[item.tag] else {
                    continue
                }
                item.representedObject = menuItem.rawValue
            }
        }
    }

    func menuWillOpen(_ menu: NSMenu) {
        guard let identifier = updateIdentifier(for: menu) else {
            return
        }
        let tracking = MainMenuUpdateRequest(identifier: identifier, menu: menu)
        NSApp.sendAction(#selector(MenuUpdateProvider.registerMenuUpdateRequest(_:)), to: nil, from: tracking)
    }

    func menuDidClose(_ menu: NSMenu) {
        guard let identifier = updateIdentifier(for: menu) else {
            return
        }
        let tracking = MainMenuUpdateRequest(identifier: identifier, menu: menu)
        NSApp.sendAction(#selector(MenuUpdateProvider.unregisterMenuUpdateRequest(_:)), to: nil, from: tracking)
    }

    ///This is a fallback function if ManuscriptWindowController is not in the responser chain. We just disable all webkit mapped items.
    @objc func registerMenuUpdateRequest(_ request: MainMenuUpdateRequest) {
        for item in request.menu.items where MainMenuItem.reservedMenuTags.contains(item.tag) {
            item.isEnabled = false
        }
    }

    @objc func validateMenuItem(_ menuItem: NSMenuItem) -> Bool {
        switch menuItem.action {
        case #selector(loginAction(_:)):
            if sessionManager.cloudSessionState.user == nil {
                return true
            } else {
                return false
            }
        case #selector(logoutAction(_:)):
            if sessionManager.cloudSessionState.user != nil {
                return true
            } else {
                return false
            }
        default:
            return menuItem.isEnabled
        }
    }

}

// MARK: - Projects List
extension AppDelegateMac {

    private func setupProjectLists() -> (AnyCancellable, AnyCancellable) {

        func addProjects(_ projects: [Project], to menu: NSMenu) {
            guard !projects.isEmpty else {
                let item = NSMenuItem(title: "None", action: nil, keyEquivalent: "")
                item.isEnabled = false
                menu.items = [item]
                return
            }
            let items = projects.map { project -> NSMenuItem in
                var title = (project.title ?? "")
                if title.isEmpty { title = "Untitled Project" }
                let item = NSMenuItem(title: title, action: #selector(AppDelegateMac.openProjectAction(_:)), keyEquivalent: "")
                item.representedObject = project
                return item
            }
            menu.items = items
        }

        let localSink =  dataManager[.local].documentServer.projectList.$projects
            .sink { [weak self] projects in
                guard let this = self else { return }
                addProjects(projects.values.sorted(by: {$0.displayTitle < $1.displayTitle}), to: this.localProjectsMenu)
            }

        let cloudSink =  dataManager[.cloud].documentServer.projectList.$projects
            .sink { [weak self] projects in
                guard let this = self else { return }
                addProjects(projects.values.sorted(by: {$0.displayTitle < $1.displayTitle}), to: this.cloudProjectsMenu)
            }

        return (localSink, cloudSink)
    }

    @IBAction func openProjectAction(_ sender: NSMenuItem) {
        guard let project = sender.representedObject as? Project else {
            return
        }

        do {
            let content = EditorContent(project: project)
            try (NSDocumentController.shared as? DocumentController)?.open(content: content)
        } catch let error {
            NSApp.presentError(error)
        }
    }

    @IBAction func newDocument(_ sender: Any) {
        NSDocumentController.shared.newDocument(sender)
    }

    // MARK: - Scripting
    func application(_ sender: NSApplication, delegateHandlesKey key: String) -> Bool {
        return key == "appName"
    }

    @objc dynamic var appName: String {
        (Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String) ?? "_unknown_"
    }

}
