//
//  ProjectWindowController.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Cocoa
import SwiftUI
import os.log
import Combine

@objc
class ProjectWindowController: NSWindowController, MenuUpdateProvider, NSWindowDelegate {

    private static let toolbarIdentifier = "main_toolbar"

    private lazy var toolbarState: ToolbarState = ToolbarState()

    private var _editorViewControllerProxy: EditorViewControllerScriptingProxy?
    @objc dynamic var editorViewControllerProxy: EditorViewControllerScriptingProxy? {
        get {
            _editorViewControllerProxy
        }
        set {
            _editorViewControllerProxy = newValue
        }
    }

    let content: EditorContent

    required init?(coder: NSCoder) {
        preconditionFailure("default initializer should never be called")
    }

    init(content: EditorContent) {
        self.content = content
        super.init(window: nil)
        createWindow()
    }

    private func createWindow() {
        if self.window == nil {
            window = NSWindow(
                contentRect: .zero,
                styleMask: [.titled, .closable, .miniaturizable, .resizable, .fullSizeContentView, .unifiedTitleAndToolbar],
                backing: .buffered, defer: false)
            window!.toolbar = EditorToolbar(identifier: Self.toolbarIdentifier, toolbarState: toolbarState)
            window?.delegate = self

            let editorViewController = EditorViewController(content: content, toolbarState: toolbarState, mainMenuState: mainMenuState)
            editorViewControllerProxy = EditorViewControllerScriptingProxy(windowController: self, editorViewController: editorViewController)

            window?.contentViewController = editorViewController
            window?.setContentSize(EditorViewController.defaultContentSize)
            window!.center()

            if let project = content.project {
                window?.setFrameAutosaveName("ProjectWindow:\(project.documentId)")
            }
        }
    }

    // MARK: - Menu Updates
    private var mainMenuStateSink: AnyCancellable?

    private lazy var mainMenuState: MainMenuState = {
        let state = MainMenuState()
        self.mainMenuStateSink = state.objectWillChange
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in
                self?.updateAllMenuState()
            }
        return state
    }()

    private var currentUpdatedMenus = Set<NSMenu>()
    private var currentSentUpdateRequests = Set<String>()

    @objc func registerMenuUpdateRequest(_ request: MainMenuUpdateRequest) {
        currentUpdatedMenus.insert(request.menu)
        //request state update it was not yet requested while the menu was still opened
        if !currentSentUpdateRequests.contains(request.updateIdentifier) {
            currentSentUpdateRequests.insert(request.updateIdentifier)
            mainMenuState.requestMenuUpdate(with: request.updateIdentifier)
        }
        //update the menu with the latest known state
        updateMenuState(request.menu)
    }

    @objc func unregisterMenuUpdateRequest(_ request: MainMenuUpdateRequest) {
        currentUpdatedMenus.remove(request.menu)
        if currentUpdatedMenus.isEmpty {
            //invalidate all update requests
            currentSentUpdateRequests.removeAll()
        }
    }

    private func updateMenuState(_ menu: NSMenu) {
        for item in menu.items {
            guard
                let menuItemRaw = item.representedObject as? String,
                let menuItem = MainMenuItem(rawValue: menuItemRaw)
            else {
                continue
            }
            let itemState = mainMenuState[menuItem]
            item.isEnabled = itemState.isEnabled
            item.state = itemState.isActive ? .on : .off
            if let titleOverride = itemState.title {
                item.title = titleOverride
            }
        }
    }

    private func updateAllMenuState() {
        for menu in currentUpdatedMenus {
            updateMenuState(menu)
        }
    }

    @IBAction func forwardedAction(_ sender: NSMenuItem) {
        print("menu action: \(String(describing: sender.representedObject))")
        guard
            let itemRaw = sender.representedObject as? String,
            let menuItem = MainMenuItem(rawValue: itemRaw)
        else {
            return
        }
        mainMenuState.signalAction(for: menuItem)
    }

    override func windowTitle(forDocumentDisplayName displayName: String) -> String {
        return content.enclave.scope == .standalone ? super.windowTitle(forDocumentDisplayName: displayName) : (content.project?.displayTitle ?? "New Project")
    }

    // MARK: - Scripting
    override var objectSpecifier: NSScriptObjectSpecifier? {
        guard
            let documentSpecifier = document?.objectSpecifier,
            let classDescription = documentSpecifier?.keyClassDescription
        else {
            return nil
        }
        let specifier = NSPropertySpecifier(containerClassDescription: classDescription, containerSpecifier: documentSpecifier, key: "mainWindowController")
        return specifier
    }

    // MARK: - Window Delegate
    func window(_ window: NSWindow, shouldPopUpDocumentPathMenu menu: NSMenu) -> Bool {
        return content.enclave.scope == .standalone
    }

}
