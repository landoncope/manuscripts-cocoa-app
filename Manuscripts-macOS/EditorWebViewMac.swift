//
//  EditorWebViewMac.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import SwiftUI
import WebKit

struct EditorWebViewMac: NSViewRepresentable {

    @EnvironmentObject var content: EditorContent
    @EnvironmentObject var toolbarState: ToolbarState
    @EnvironmentObject var mainMenuState: MainMenuState
    @EnvironmentObject var nativeBridge: NativeBridge

    @ObservedObject var dataManager = AppEnvironment.shared.dataManager
    @ObservedObject var sessionManager = AppEnvironment.shared.sessionManager

    @Binding var webViewLoaded: Bool

    func makeCoordinator() -> EditorWebViewCoordinator {
        let coordinator = EditorWebViewCoordinator(content: content, mainMenuState: mainMenuState, toolbarState: toolbarState, webViewLoaded: $webViewLoaded)
        //make the coordinator responsible for native bridge handling
        nativeBridge.nativeBridgeProvider = coordinator
        return coordinator
    }

    func makeNSView(context: NSViewRepresentableContext<EditorWebViewMac>) -> WKWebView {
        let webView = context.coordinator.ensureWebView()
        return webView
    }

    func updateNSView(_ webView: WKWebView, context: NSViewRepresentableContext<EditorWebViewMac>) {
        //noop ATM but needs to be here to satisfy protocol reqs
    }
}

#if DEBUG

struct EditorWebViewPreview: View {
    @State var webViewLoaded = false

    var body: some View {
        EditorWebViewMac(webViewLoaded: $webViewLoaded)
    }
}

struct EditorWebViewPreviews: PreviewProvider {
    static var previews: some View {
        EditorWebViewPreview()
            .environmentObject(EditorContent(enclave: AppEnvironment.shared.dataManager[.local]))
            .environmentObject(ToolbarState())
            .environmentObject(MainMenuState())
    }
}
#endif
