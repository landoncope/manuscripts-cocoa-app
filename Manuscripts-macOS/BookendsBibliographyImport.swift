//
//  BookendsImportProvider.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import SwiftUI
import Combine

/**
 Concrete `BibliographyImport`.
 Uses OSA XPC helper to get a RIS file from Bookends and then processes this file.

 Also implements `InteractiveTaskProvider` to work together with an `BibliographyImportInteractiveTask`.
 */
class BookendsBibliographyImport: BibliographyImport {

    static let risChunkMarker = "TY  -".data(using: .utf8)!

    static var supportsSafeMode: Bool { true }

    let canRetry = true

    private func importResult(from chunks: [FilterProcess.FileChunkResult]) -> BibliogaphyImportResult {
        var imported = [Data]()
        var failed = [Error]()
        for chunk in chunks {
            switch chunk {
            case let .success(data):
                imported.append(data)
            case let .failure(error):
                failed.append(error)
            }
        }
        return BibliogaphyImportResult(format: .citeprocJson, importedItems: imported, importErrors: failed)
    }

    private var pandocFilter: FilterProcess?

    func importLibrary(from fileUrl: URL, progress: Progress? = nil, options: BibliographyImportOptions = [], completion: @escaping BibliographyImportCompletion) {
        isCancelled = false
        performImportLibrary(from: fileUrl, progress: progress, options: options, completion: completion)
    }

    private func performImportLibrary(from fileUrl: URL, progress: Progress? = nil, options: BibliographyImportOptions = [], completion: @escaping BibliographyImportCompletion) {
        guard !isCancelled else { return }
        guard let pandocUrl = Bundle.main.url(forAuxiliaryExecutable: "pandoc-citeproc") else {
            preconditionFailure("embedded executable not found")
        }

        let pandocArgumnets: [String] = ["--bib2json", "-fRIS"]
        pandocFilter = FilterProcess(executableUrl: pandocUrl, arguments: pandocArgumnets)
        pandocFilter?.lineEnd = .classicMac

        if options.contains(.safeMode) {
            pandocFilter?.chunkRecogniser = { data in
                return data.prefix(Self.risChunkMarker.count) == Self.risChunkMarker
            }
        }

        pandocFilter?.process(fileUrl: fileUrl, progress: progress) { (result) in
            guard !self.isCancelled else { return }

            switch result {
            case let .failure(error):
                completion(.failure(BibliographyImportError.wrapErrorConditionally(error)))
                return
            case let .success(chunks):
                if !options.contains(.safeMode), let firstChunk = chunks.first, case let .failure(chunkError) = firstChunk {
                    completion(.failure(BibliographyImportError.wrapErrorConditionally(chunkError)))
                } else {
                    completion(.success(self.importResult(from: chunks)))
                }
            }

            self.pandocFilter = nil
        }
    }

    func importLibrary(progress: Progress? = nil, options: BibliographyImportOptions = [], completion: @escaping BibliographyImportCompletion) {
        isCancelled = false

        var osaProgress: Progress?
        var importProgress: Progress?

        if let parentProgress = progress {
            parentProgress.totalUnitCount = 1000
            osaProgress = Progress(totalUnitCount: 1000, parent: parentProgress, pendingUnitCount: 500)
            importProgress = Progress(totalUnitCount: 1000, parent: parentProgress, pendingUnitCount: 500)
        }

        let connection = NSXPCConnection(serviceName: osaHelperServiceName)
        connection.remoteObjectInterface = NSXPCInterface(with: OSAHelperProtocol.self)
        connection.resume()

        let service = connection.remoteObjectProxyWithErrorHandler { error in
            completion(.failure(BibliographyImportError.wrapErrorConditionally(error)))
        }

        guard let osaService = service as? OSAHelperProtocol else {
            preconditionFailure("unexpected OSA service protocol")
        }

        let tempFileUrl = FileManager.default.temporaryDirectory.appendingPathComponent(UUID().uuidString).appendingPathExtension("ris")

        osaService.importBookendsLibrary(fileUrl: tempFileUrl) { (error) in
            osaProgress?.completedUnitCount = osaProgress?.totalUnitCount ?? 0

            defer { self.cleanup() }

            guard !self.isCancelled else {
                return
            }

            guard error == nil else {
                completion(.failure(BibliographyImportError.wrapErrorConditionally(error!)))
                return
            }

            self.performImportLibrary(from: tempFileUrl, progress: importProgress, options: options, completion: completion)
        }
    }

    private var isCancelled = false
    private var tempFileUrl: URL?

    private func cleanup() {
        if let url = tempFileUrl {
            try? FileManager.default.removeItem(at: url)
            tempFileUrl = nil
        }
    }

    func cancel() {
        self.isCancelled = true
        pandocFilter?.cancel()
        cleanup()
    }

    private var applySink: AnyCancellable?

    //This method might potentially be moved to the BibliographyImport protocol extension with only a thin callback to perform a single data import via the native bridge and for bulk merge
    func applyImportResult(_ importResult: BibliogaphyImportResult, nativeBridge: NativeBridge, progress: Progress?, options: BibliographyImportOptions, completion: @escaping BibliographyApplyCompletion) {
        guard !importResult.importedItems.isEmpty else { return }

        var dataToImport: [Data]
        if options.contains(.preferBulkMode) {
            dataToImport = mergeImportedItems(importResult.importedItems)
        } else {
            dataToImport = importResult.importedItems
        }

        progress?.totalUnitCount = Int64(dataToImport.count)

        var iterator = dataToImport.makeIterator()

        let subject = PassthroughSubject<Data, Never>()
        var results = [NativeBridgeResult]()

        applySink = subject
            .flatMap { data in
                Future<NativeBridgeResult, Never> { promise in
                    nativeBridge.importBibliography(uuid: UUID(), data: data, format: .citeprocJson) { result in
                        promise(.success(result))
                    }
                }
            }
            .receive(on: DispatchQueue.main) //avoids a deadlock
            .sink { [weak self ] result in
                results.append(result)
                progress?.completedUnitCount += 1
                if let nextData = iterator.next() {
                    subject.send(nextData)
                } else {
                    completion(.success(results))
                    self?.applySink = nil
                }
            }
        //process items one by one
        subject.send(iterator.next()!)
    }

    private func mergeImportedItems(_ items: [Data]) -> [Data] {
        guard items.count > 1 else { return items }

        let mergedArray = items.flatMap { (item) -> [Any] in
            guard let parsed = try? JSONSerialization.jsonObject(with: item, options: []) as? [Any] else {
                return []
            }
            return parsed
        }
        guard let resultData = try? JSONSerialization.data(withJSONObject: mergedArray, options: []) else {
            preconditionFailure("could not JSON encode JSON decoded data")
        }
        return [resultData]
    }

}

extension BookendsBibliographyImport: InteractiveTaskProvider {
    var sheetView: AnyView {
        return AnyView(BibliographyImportView())
    }

    func canSafelyRetry(error: Error) -> Bool {
        switch error {
        case let BibliographyImportError.importError(underlayingError: underlaying):
            return underlaying is FilterProcess.FilterError
        default:
            return false
        }
    }

    func describeError(_ error: Error, detailed: Bool = false) -> String? {
        switch error {
        case let BibliographyImportError.importError(underlayingError: underlaying):
            switch underlaying {
            case FilterProcess.FilterError.filterError:
                return self.describeError(underlaying, detailed: detailed)
            default:
                return nil
            }
        case let FilterProcess.FilterError.filterError(chunkData, errorData):
            if detailed {
                var result = "----- failed data chunk -----\n"
                result += String(data: chunkData, encoding: .utf8) ?? "(invaid chunk data)"
                result += "\n----- reported error -----\n"
                result += String(data: errorData, encoding: .utf8) ?? "(invalid error data)"
                return result
            } else {
                return String(data: errorData, encoding: .utf8) ?? "(invalid error data)"
            }
        case let native as NativeBridgeError:
            switch native {
            case .timeout:
                return "JavaScript task timed out"
            case let .javaScriptError(error):
                return "JavaScript error: \(error.localizedDescription)"
            case let .taskError(message):
                return "JavaScript task error: \(message)"
            }
        default:
            return nil
        }
    }
}
