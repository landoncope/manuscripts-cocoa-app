//
//  NSView+Manuscripts.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import AppKit

extension NSView {
    func firstDescendant<T: NSView>(of type: T.Type) -> T? {
        //shallow search first
        if let found = subviews.first(where: {$0 is T}) {
            return found as? T
        }
        for subview in subviews {
            if let found = subview.firstDescendant(of: type) {
                return found
            }
        }
        return nil
    }
}
