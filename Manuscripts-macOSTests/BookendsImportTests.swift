//
//  BookendsImportTests.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
import Combine
@testable import Manuscripts_macOS

class BookendsImportTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    private var importerSink: AnyCancellable?

    func testImporterSmallFile() {
        let importer = BookendsBibliographyImport()

        guard let fileUrl = Bundle(for: Self.self).url(forResource: "sample", withExtension: "ris") else {
            XCTFail("file not found")
            return
        }

        let expect = expectation(description: "expect import")
        importer.importLibrary(from: fileUrl) { (result) in
            switch result {
            case let .success(importResult):
                XCTAssertNotNil(importResult.importedItems)
                XCTAssertEqual(importResult.importedItems.count, 1)
            case let .failure(error):
                XCTFail("unexpected error: \(error.localizedDescription)")
            }
            expect.fulfill()
        }

        waitForExpectations(timeout: 1000)
    }

    func testImporterSmallFileChunked() {
        let importer = BookendsBibliographyImport()

        guard let fileUrl = Bundle(for: Self.self).url(forResource: "sample", withExtension: "ris") else {
            XCTFail("file not found")
            return
        }

        let expect = expectation(description: "expect import")
        importer.importLibrary(from: fileUrl, options: [.safeMode]) { result in
            switch result {
            case let .success(importResult):
                XCTAssertNotNil(importResult.importedItems)
                XCTAssertEqual(importResult.importedItems.count, 5)
            case let .failure(error):
                XCTFail("unexpected error: \(error.localizedDescription)")
            }
            expect.fulfill()
        }

        waitForExpectations(timeout: 1000)
    }

    func testImporterBigFile() {
        let importer = BookendsBibliographyImport()

        guard let fileUrl = Bundle(for: Self.self).url(forResource: "sample-big", withExtension: "ris") else {
            XCTFail("file not found")
            return
        }

        let expect = expectation(description: "expect import")
        importer.importLibrary(from: fileUrl) { result in
            switch result {
            case let .success(importResult):
                XCTAssertNotNil(importResult.importedItems)
                XCTAssertEqual(importResult.importedItems.count, 1)
            case let .failure(error):
                XCTFail("unexpected error: \(error.localizedDescription)")
            }
            expect.fulfill()
        }

        waitForExpectations(timeout: 1000)
    }

    func testImporterBigFileChunked() {
        let importer = BookendsBibliographyImport()

        guard let fileUrl = Bundle(for: Self.self).url(forResource: "sample-big", withExtension: "ris") else {
            XCTFail("file not found")
            return
        }

        let expect = expectation(description: "expect import")
        importer.importLibrary(from: fileUrl, options: [.safeMode]) { result in
            switch result {
            case let .success(importResult):
                XCTAssertNotNil(importResult.importedItems)
                XCTAssertEqual(importResult.importedItems.count, 200)
            case let .failure(error):
                XCTFail("unexpected error: \(error.localizedDescription)")
            }
            expect.fulfill()
        }

        waitForExpectations(timeout: 1000)
    }

    func testImporterProblematic() {
        let importer = BookendsBibliographyImport()

        guard let fileUrl = Bundle(for: Self.self).url(forResource: "problematic", withExtension: "ris") else {
            XCTFail("file not found")
            return
        }

        let expect = expectation(description: "expect import")
        importer.importLibrary(from: fileUrl) { result in
            switch result {
            case .success:
                XCTFail("unsafe problematic import should fail")
            case .failure:
                break //just ok
            }
            expect.fulfill()
        }

        waitForExpectations(timeout: 1000)
    }

    func testImporterProblematicChunked() {
        let importer = BookendsBibliographyImport()

        guard let fileUrl = Bundle(for: Self.self).url(forResource: "problematic", withExtension: "ris") else {
            XCTFail("file not found")
            return
        }

        let expect = expectation(description: "expect import")
        importer.importLibrary(from: fileUrl, options: [.safeMode]) { result in
            switch result {
            case let .success(importResult):
                XCTAssertFalse(importResult.importedItems.isEmpty)
                XCTAssertFalse(importResult.importErrors.isEmpty)
            case .failure:
                XCTFail("safe mode problematic import should not fail")
            }
            expect.fulfill()
        }

        waitForExpectations(timeout: 1000)
    }

    func testImportLibrary() {
        let importer = BookendsBibliographyImport()
        let expect = expectation(description: "expect bookends import")
        importerSink = importer.importLibraryPublisher(timeout: 60, retries: 0)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .failure(let error):
                    XCTFail("import error: \(error)")
                default:
                    break
                }
                expect.fulfill()
            }, receiveValue: { importResult in
                XCTAssertFalse(importResult.importedItems.isEmpty)
                XCTAssertTrue(importResult.importErrors.isEmpty)
            })

        waitForExpectations(timeout: 1000) { (error) in
            if let actualError = error {
                XCTFail("expectation failed: \(actualError.localizedDescription)")
            }
        }
    }

}
