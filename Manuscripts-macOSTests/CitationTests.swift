//
//  CitationTests.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
@testable import Manuscripts_macOS

class CitationTests: XCTestCase, NativeBridgeProvider {

    override func setUp() {
        continueAfterFailure = false
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    // Native Bridge requirement
    func importBibliography(uuid: UUID, data: Data, format: BibliographyDataFormat, completion: @escaping NativeBridgeCompletion) {
        //noop
    }

    func openAnyDocument() -> ProjectDocument {
        let dataManager = AppEnvironment.shared.dataManager
        XCTAssertNotNil(dataManager)

        let localEnclave = dataManager![.local]
        let projectList = localEnclave.documentServer.projectList
        XCTAssertNotNil(projectList)

        let project = projectList?.projects.values.first
        XCTAssertNotNil(project, "to run the test the local enclave has to contain at least one project")

        let content = EditorContent(project: project!)
        let documentController = NSDocumentController.shared as? DocumentController
        XCTAssertNotNil(documentController)

        let document = try? documentController!.open(content: content) as? ProjectDocument
        XCTAssertNotNil(document)

        return document!
    }

    private var defautScriptingProvider: NativeBridgeProvider?
    private var scriptExecExpactation: XCTestExpectation?

    func testCitation() {
        let document = openAnyDocument()

        let editorScripting = document.mainWindowController?.editorViewControllerProxy?.editorViewController.nativeBridge
        XCTAssertNotNil(editorScripting)

        defautScriptingProvider = editorScripting?.nativeBridgeProvider
        XCTAssertNotNil(defautScriptingProvider)

        //inject self as a scripting provider
        editorScripting?.nativeBridgeProvider = self

        for baseName in ["citation-001", "citation-001-compiled"] {
            //load the script
            let url = Bundle(for: Self.self).url(forResource: baseName, withExtension: "scpt")
            XCTAssertNotNil(url)
            let script = NSAppleScript(contentsOf: url!, error: nil)
            XCTAssertNotNil(script)

            scriptExecExpactation = expectation(description: "wait for the cite command")
            var errorDict: NSDictionary?

            DispatchQueue.main.async {
                script!.executeAndReturnError(&errorDict)
                XCTAssertNil(errorDict)
            }

            waitForExpectations(timeout: 10) { (error) in
                XCTAssertNil(error)
            }
        }
    }

    // MARK: - Scripting Provider
    func cite(citation: String, type: String) {
        defautScriptingProvider?.cite(citation: citation, type: type)
        XCTAssertEqual(citation, "test_cite")
        scriptExecExpactation?.fulfill()
    }
}
