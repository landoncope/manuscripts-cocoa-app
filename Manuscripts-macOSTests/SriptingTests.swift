//
//  SriptingTests.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
@testable import Manuscripts_macOS

class SriptingTests: XCTestCase {

    var scriptWrapper: AppleScriptWrapper!

    override func setUp() {
        guard let url = Bundle(for: Self.self).url(forResource: "calling-convention", withExtension: "scpt") else {
            preconditionFailure("script not found")
        }
        do {
            scriptWrapper = try AppleScriptWrapper(scriptUrl: url)
        } catch let error {
            preconditionFailure("can not load script: \(error)")
        }
    }

    func testNoParam() {
        do {
            let pingValue = "magicValue"
            let returnValue = try scriptWrapper!.call(function: "pingEmpty")
            guard let stringValue = returnValue.stringValue else {
                XCTFail("got unexpected response")
                return
            }
            XCTAssertEqual(pingValue, stringValue)
        } catch let error {
            XCTFail("AppleScript execution failed: \(error)")
        }
    }

    func testPlainParam() {
        do {
            let pingValue = "ping"
            let returnValue = try scriptWrapper!.call(function: "pingPlain", directObject: [pingValue])
            guard let stringValue = returnValue.stringValue else {
                XCTFail("got unexpected response")
                return
            }
            XCTAssertEqual(pingValue, stringValue)
        } catch let error {
            XCTFail("AppleScript execution failed: \(error)")
        }
    }

    func testPlainTwoParams() {
        do {
            let pingValue1 = "ping"
            let pingValue2 = "PONG"
            let returnValue = try scriptWrapper!.call(function: "pingPlain2", directObject: [pingValue1, pingValue2])
            guard let stringValue = returnValue.stringValue else {
                XCTFail("got unexpected response")
                return
            }
            XCTAssertEqual(pingValue1 + pingValue2, stringValue)
        } catch let error {
            XCTFail("AppleScript execution failed: \(error)")
        }
    }

    func testLabelParam() {
        do {
            let pingValue = "ping"
            let returnValue = try scriptWrapper!.call(function: "pingLabel", keywordParams: [.for: pingValue])
            guard let stringValue = returnValue.stringValue else {
                XCTFail("got unexpected response")
                return
            }
            XCTAssertEqual(pingValue, stringValue)
        } catch let error {
            XCTFail("AppleScript execution failed: \(error)")
        }
    }

    func testLabelTwoParams() {
        do {
            let pingValueFrom = Int32.random(in: 1...10)
            let pingValueTo = Int32.random(in: 100...200)
            let returnValue = try scriptWrapper!.call(function: "pingLabel2", keywordParams: [.from: pingValueFrom, .to: pingValueTo])
            let int32Value = returnValue.int32Value
            XCTAssertEqual(pingValueTo - pingValueFrom, int32Value)
        } catch let error {
            XCTFail("AppleScript execution failed: \(error)")
        }
    }

    func testGivenParam() {
        do {
            let pingValue = "ping"
            let returnValue = try scriptWrapper!.call(function: "pingGiven", userParams: ["pingValue": pingValue])
            guard let stringValue = returnValue.stringValue else {
                XCTFail("got unexpected response")
                return
            }
            XCTAssertEqual(pingValue, stringValue)
        } catch let error {
            XCTFail("AppleScript execution failed: \(error)")
        }
    }

    func testGivenTwoParams() {
        do {
            let pingValue1 = "ping"
            let pingValue2 = "PONG"
            let returnValue = try scriptWrapper!.call(function: "pingGiven2", userParams: ["pingValue1": pingValue1, "pingValue2": pingValue2])
            guard let stringValue = returnValue.stringValue else {
                XCTFail("got unexpected response")
                return
            }
            XCTAssertEqual(pingValue1 + pingValue2, stringValue)
        } catch let error {
            XCTFail("AppleScript execution failed: \(error)")
        }
    }

    func testMixedParams() {
        do {
            let prefix = "test"
            let pingValueFrom = Int32.random(in: 1...3)
            let pingValueTo = Int32.random(in: (pingValueFrom + 1)...(pingValueFrom + 5))
            let pingValue1 = "ping"
            let pingValue2 = "PONG"
            let returnValue = try scriptWrapper!.call(function: "pingMixed", directObject: [prefix], keywordParams: [.on: prefix, .from: pingValueFrom, .to: pingValueTo], userParams: ["pingValue1": pingValue1, "pingValue2": pingValue2])
            guard let stringValue = returnValue.stringValue else {
                XCTFail("got unexpected response")
                return
            }

            var expectedValue = prefix
            for _ in pingValueFrom...pingValueTo {
                expectedValue += pingValue1 + pingValue2
            }
            XCTAssertEqual(expectedValue, stringValue)
        } catch let error {
            XCTFail("AppleScript execution failed: \(error)")
        }
    }

    /*
    func testInterLeavedParams() {
           do {
            let pingValue = "ping"
            let pongValue = "pong"
            let returnValue = try scriptWrapper!.call(function: "ping:pong", userParams: ["ping": pingValue, "pong": pongValue])
               guard let stringValue = returnValue.stringValue else {
                   XCTFail("got unexpected response")
                   return
               }
               XCTAssertEqual(pingValue, stringValue)
           } catch let error {
               XCTFail("AppleScript execution failed: \(error)")
           }
       }
     */

}
