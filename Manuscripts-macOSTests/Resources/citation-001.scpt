tell application id "com.manuscripts.Manuscripts"
    set controller to get editor view controller of main window controller of document 1
    tell controller
        cite with citation string "test_cite" in Citations XML format
    end tell
end tell
