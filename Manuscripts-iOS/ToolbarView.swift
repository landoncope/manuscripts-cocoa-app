//
//  ToolbarView.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import SwiftUI

struct ToolbarSegment: View {

    @EnvironmentObject var toolbarState: ToolbarState
    var items: [ToolbarItem]

    var body: some View {
        HStack(spacing: -1) {
            ForEach(items, id: \.self) { item -> ToolbarButton in
                var buttonPosition: ToolbarButton.ButtonPosition = []
                if item == self.items.first! { buttonPosition.insert(.leading) }
                if item == self.items.last! { buttonPosition.insert(.trailing) }

                return ToolbarButton(buttonPosition: buttonPosition, toolbarItem: item, itemState: self.toolbarState[item])
            }
        }
        .background(Color.clear)
    }
}

struct ToolbarView: View {

    @EnvironmentObject var toolbarState: ToolbarState

    var body: some View {
        HStack(spacing: 12) {
            ForEach(ToolbarItem.groups.map({$1}), id: \.items) { group in
                ToolbarSegment(items: group.items)
            }
        }.buttonStyle(BorderlessButtonStyle())
    }
}

#if DEBUG
struct ToolbarViewPreviews: PreviewProvider {
    static var previews: some View {
        ToolbarView()
            .environmentObject(ToolbarState())
            .border(Color.black)
    }
}
#endif
