//
//  OSAHelper.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import os.log
import AppKit

/**
 Provides individual call for various Apple Script actions. Exported as XPC object.
 */
@objc class OSAHelper: NSObject, OSAHelperProtocol {

    // MARK: - Debug
    @objc func upperCase(string: String, withReply reply: @escaping (String) -> Void) {
        reply(string.uppercased())
    }

    // MARK: - Bookends
    static let bookendsBundleIds = [ "com.sonnysoftware.bookends", "com.sonnysoftware.bookends.mas"]

    var installedBookendsIdentifier: String? {
        return Self.bookendsBundleIds.first { (candidate) -> Bool in
            return NSWorkspace.shared.urlForApplication(withBundleIdentifier: candidate) != nil
        }
    }

    func importBookendsLibrary(fileUrl: URL, handler: @escaping BibliographyImportXPCHandler) {
        DispatchQueue.main.async {
            self.importBookendsLibraryGated(fileUrl: fileUrl, handler: handler)
        }
    }

    private func importBookendsLibraryGated(fileUrl: URL, handler: @escaping BibliographyImportXPCHandler) {
        guard let bookendsBundleId = installedBookendsIdentifier else {
            os_log(.error, "Bookends application not found")
            handler(OSAHelperError(code: .applicationNotFound))
            return
        }
        guard let scriptUrl = Bundle.main.url(forResource: "ImportBookendsLibrary", withExtension: "scpt") else {
            os_log(.error, "script file not found: ImportBookendsLibrary")
            handler(OSAHelperError(code: .fileNotFound))
            return
        }
        do {
            //let temporaryFileUrl = FileManager.default.temporaryDirectory.appendingPathComponent(UUID().uuidString).appendingPathExtension("ris")
            let temporaryFileUrl = fileUrl
            let wrapper = try AppleScriptWrapper(scriptUrl: scriptUrl)
            let resultDescriptor = try wrapper.call(function: "importBookendsLibrary", directObject: [temporaryFileUrl.path, bookendsBundleId])
            guard resultDescriptor.booleanValue else {
                os_log(.error, "script did not return the expected true value")
                handler(OSAHelperError(code: .unexpectedResult))
                return
            }
            handler(nil)
        } catch let error {
            os_log(.error, "script error: %{public}@", error.localizedDescription)
            handler(OSAHelperError(code: .scriptError))
        }
    }

}
