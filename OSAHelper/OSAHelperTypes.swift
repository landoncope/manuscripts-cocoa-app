//
//  OSAHelperProtocol.swift
//  Manuscripts
//
//  Created by Manuscripts.io team on 08/08/2018.
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

let osaHelperServiceName = "com.manuscripts.Manuscripts.OSAHelper"

typealias BibliographyImportXPCHandler = (_ error: OSAHelperError?) -> Void

@objc (OSAHelperError)
class OSAHelperError: NSError, RetryableError {

    static let domain = "OSAHelperError"

    enum Code: Int {
        case fileNotFound = 1000
        case scriptError = 1001
        case unexpectedResult = 1002
        case applicationNotFound = 1003
    }

    convenience init(code: Code, userInfo: [String: Any]? = nil) {
        self.init(domain: Self.domain, code: code.rawValue, userInfo: userInfo)
    }

    /// Required by XPC
    override class var supportsSecureCoding: Bool { true }

    var isRetryable: Bool {
        switch self.code {
        case Code.scriptError.rawValue:
            return true
        default:
            return false
        }
    }
}

@objc protocol OSAHelperProtocol {
    func importBookendsLibrary(fileUrl: URL, handler: @escaping BibliographyImportXPCHandler)
}
